let bcrypt = require('bcryptjs');
let jwt = require('jsonwebtoken');
let CONFIG = require('../Config');
let datetime = require('node-datetime');
let Service = require('../Services');
let Model = require('../Models');
let Boom = require('boom');
let Joi = require('joi');
let UploadMultipart = require('../Lib/UploadMultiPart')

const sendSuccess = function (successMsg, data) {
    successMsg = successMsg || CONFIG.appConstants.STATUS_MSG.REQUEST_SUCCESS;
    if (typeof successMsg === 'object' && successMsg.hasOwnProperty('statusCode') && successMsg.hasOwnProperty('customMessage')) {
        return {statusCode:successMsg.statusCode, message: successMsg.customMessage, data: data || null};
    }else {
        return {statusCode:200, message: successMsg, data: data || null};
    }
};

const sendError = function (data) {
    try {
        if (typeof data === 'object' && data.hasOwnProperty('statusCode') && data.hasOwnProperty('customMessage')) {
            let errorToSend = Boom.create(data.statusCode,data.customMessage);
            errorToSend.output.payload.responseType = data.type;
            return errorToSend;
        } else {
            let errorToSend = '';
            if (typeof data === 'object') {
                if (data.name === 'MongoError') {
                    errorToSend += CONFIG.appConstants.STATUS_MSG.DB_ERROR.customMessage
                    if (data.code === 11000) {
                         let duplicateValue = data.errmsg && data.errmsg.substr(data.errmsg.lastIndexOf('{ : "') + 5);
                         duplicateValue = duplicateValue.replace('}','');
                         errorToSend += CONFIG.appConstants.STATUS_MSG.DUPLICATE.customMessage + " : " + duplicateValue;
                        console.log("==================errorToSend==================",data.message)
                        if (data.message.indexOf('email_1')>-1){
                            errorToSend = CONFIG.appConstants.STATUS_MSG.EMAIL_ALREADY_EXIST.customMessage;
                        }
                        errorToSend = CONFIG.appConstants.STATUS_MSG.DUPLICATE.customMessage + " : " + duplicateValue;
                    }
                } else if (data.name === 'ApplicationError') {
                    errorToSend += CONFIG.appConstants.STATUS_MSG.APP_ERROR.customMessage + ' : ';
                } else if (data.name === 'ValidationError') {
                    errorToSend += CONFIG.appConstants.STATUS_MSG.APP_ERROR.customMessage + data.message;
                } else if (data.name === 'CastError') {
                    errorToSend += CONFIG.appConstants.STATUS_MSG.DB_ERROR.customMessage + CONFIG.appConstants.STATUS_MSG.INVALID_EMAIL.customMessage + data.value;
                }
            } else {
                errorToSend = data
            }
            let customErrorMessage = errorToSend;
            if (typeof customErrorMessage === 'string'){
                if (errorToSend.indexOf("[") > -1) {
                    customErrorMessage = errorToSend.substr(errorToSend.indexOf("["));
                }
                customErrorMessage = customErrorMessage && customErrorMessage.replace(/"/g, '');
                customErrorMessage = customErrorMessage && customErrorMessage.replace('[', '');
                customErrorMessage = customErrorMessage && customErrorMessage.replace(']', '');
            }
            return Boom.create(400,customErrorMessage)
        }
    }
    catch (e) {
        console.log(e);
        return e
    }

};

async function encryptPassword(password)
{
    let salt = await bcrypt.genSalt(10);
    password = await bcrypt.hash(password,salt);
    return password
}

async function storeOtp(otp, payload)
{
    try
    {
        let email = payload || null
        let phone = payload.phone || null
        if(phone == null)
        {
            let dataToSave = {
                otp : otp,
                email : email,
                isUsed : false
            }

            await Service.queries.savedata(Model.Otp,dataToSave);
            return CONFIG.appConstants.STATUS_MSG.EMAIL_SENT_SUCCESS
        }   
        else
        {
            let dataToSave = {
                otp : otp,
                phone : payload.phone,
                countryCode : payload.countryCode
            }
            await Service.queries.savedata(Model.Otp,dataToSave);
            return CONFIG.appConstants.STATUS_MSG.PHONE_SENT_SUCCESS
        }
    }
    catch(err)
    {
        console.log(err);
    }
}
   
async function generateOtp()
{
    const dt = datetime.create();
    let otp = dt.format('M:S');
    const randomvalue = Math.random() * (99 - 10) + 10
    const value = Math.round(randomvalue);
    
    otp = value + otp.replace(":","");

    return otp
}

async function generateToken(id,type)
{
    userId = {
        user : {
            id : id,
            type : type
        }
    }
    let token = await jwt.sign(userId, CONFIG.appConstants.SERVER.JWT_SECRET_KEY);
    console.log(token)
    return token   
}

async function getCartTotal(cart)
{
    let discounted_total
    let original_total
    cart.item.map((cartContain)=> {
            discounted_total = discounted_total + cartContain.discounted_price
            original_total = original_total + cartContain.original_price
    })
    let responseData = {
        discounted_total : discounted_total,
        original_total : original_total
    }
    return responseData
}

async function getProductTotal(productid)
{
    let date = new Date();
    console.log(date);
    let query = {
         expiry_date : { $gte : date },
         productid : productid
    }
    let collectionsOption = {
        path : 'productid',
        select : ["price","quantity"]
    }
    let discount = await Service.queries.findOnePopulateData(Model.Discount,query,{},{ sort : { _id : -1 } },collectionsOption);
    if(discount)
    {
        if(discount.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT)
        {
            if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER)
            { 
                let total = {
                    discounted_price : discount.productid.price - discount.offer,
                    original_price : discount.productid.price
                }
                return total
            }
            if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE)
            {
                let discounted_price = (discount.productid.price * discount.offer) / 100
                let total = {
                    discounted_price : discount.productid.price - discounted_price,
                    original_price : discount.productid.price
                }
                return total
            }
        }
    }
    else
    {
        console.log("In else");
        let product = await Service.queries.findOne(Model.Product,{ _id : productid },{});
        let query = {
            expiry_date : { $gte : date },
            subcategoryid : product.subcategoryid
        }
        discount = await Service.queries.findOne(Model.Discount,query,{},{ sort : { _id : -1} })
        console.log(discount);
        if(discount)
        {   
            if(discount.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT)
            {
                if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER)
                {
                    let total = {
                        discounted_price : product.price - discount.offer,
                        original_price : product.price
                    }
                    return total
                }
                if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE)
                {
                    let discounted_price = (product.price * discount.offer) / 100
                    let total = {   
                    discounted_price :product.price - discounted_price,
                    original_price : product.price
                    }
                    return total
                }
            }
        }
        else
        {
            let product = await Service.queries.findOne(Model.Product,{ _id : productid },{});
            let query = {
                expiry_date : { $gte : date },
                categoryid : product.categoryid
            }
            discount = await Service.queries.findOne(Model.Discount,query,{},{ sort : { _id : -1} })
            if(discount)
            {
                if(discount.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT)
                {
                    if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER)
                    {
                        let total = {
                            discounted_price : product.price - discount.offer,
                            original_price : product.price
                        }
                    }
                    if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE)
                    {
                        let discounted_price = (product.price * discount.offer) / 100
                        let total = {   
                        discounted_price : product.price - discounted_price,
                        original_price : product.price
                        }
                        return total
                    }
                }
            }
            else if(discount==null)
            {
                console.log("Store discount");
                let product = await Service.queries.findOne(Model.Product,{ _id : productid },{});
                let query = {
                    expiry_date : { $gte : date },
                    storeid : product.storeid
                }
                discount = await Service.queries.findOne(Model.Discount,query,{},{ sort : { _id : -1} })   
                if(discount)
                {
                    if(discount.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT)
                    {
                        if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER)
                        {
                            let total = {
                                discounted_price : product.price - discount.offer,
                                original_price : product.price
                            }
                        }
                        if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE)
                        {
                            let discounted_price = (product.price * discount.offer) / 100
                            let total = {   
                            discounted_price : product.price - discounted_price,
                            original_price : product.price
                            }
                            return total
                        }
                    }   
                }
            }
            {
                let total = {
                    discounted_price : product.price,
                    original_price : product.price
                }
                return total
            }
        }
    }
}

async function getOrderTotal(discount_code,cartTotal)
{
    if(discount_code)
    {
        let date = new Date();
        let query = {
            expiry_date : { $gte : date },
            discount_code : discount_code
        }
        let discount = await Service.queries.findOne(Model.Discount,query,{},{});
        if(discount)
        {
            if(discount.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONORDER)
            {
                if(discount.minimum_order_amount > cartTotal)
                {
                    console.log("minimum order amount:-"+discount.minimum_order_amount);
                    console.log("cart total : -"+cartTotal)
                    let successMsg = CONFIG.appConstants.STATUS_MSG.MINIMUM_ORDER_AMOUNT_REQUIRED
                    let responseData = {
                        statusCode : successMsg.statusCode,
                        customMessage : successMsg.customMessage + " " + discount.minimum_order_amount,
                    }
                    return responseData
                }
                else
                {
                    if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER)
                    {
                        let total = {
                            discounted_total : cartTotal - discount.offer,
                            original_total : cartTotal
                        }
                        return total
                    }
                    if(discount.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE)
                    {
                        let total = {
                            discounted_total : cartTotal - (cartTotal * discount.offer) / 100,
                            original_total : cartTotal
                        }
                        return total
                    }
                }
            }
        }
        else
        {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_DISCOUNT_CODE)

            // let total = {
            //     discounted_total : cartTotal,
            //     original_total : cartTotal
            // }
            // return total
        }
    }
    else
    {
        let total = {
            discounted_total : cartTotal,
            original_total : cartTotal
        }
        return total
    }
}

async function uploadImage(image){

    if (Array.isArray(image)) {
        return new Promise((resolve, reject) => {
            let imageData = [], len = image.length, count = 0;
            image.map((obj) => {
                UploadMultipart.uploadFilesOnS3(obj, (err, result) => {
                    count++;
                    imageData.push(result);
                    if (count === len)
                        resolve(imageData)
                })
            })
        });
    } else {
        return new Promise((resolve, reject) => {
            UploadMultipart.uploadFilesOnS3(image, (err, result) => {
                if (err) reject(err);
                else resolve(result)
            })
        });
    }
}

const failActionFunction = function (request, h, error) {

    console.log(".............fail action.................",error.output.payload.message);
    let customErrorMessage = '';
    if (error.output.payload.message.indexOf("[") > -1) {
        customErrorMessage = error.output.payload.message.substr(error.output.payload.message.indexOf("["));
    } else {
        customErrorMessage = error.output.payload.message;
    }
    customErrorMessage = customErrorMessage.replace(/"/g, '');
    customErrorMessage = customErrorMessage.replace('[', '');
    customErrorMessage = customErrorMessage.replace(']', '');
    error.output.payload.message = customErrorMessage;
    delete error.output.payload.validation;
    return error;
}

const authorizationHeaderObj = Joi.object({
    authorization: Joi.string().required()
}).unknown();


module.exports = {
    encryptPassword : encryptPassword,
    generateToken : generateToken,
    generateOtp : generateOtp,
    storeOtp : storeOtp,
    getProductTotal : getProductTotal,
    getCartTotal : getCartTotal,
    getOrderTotal : getOrderTotal,
    sendSuccess : sendSuccess,
    sendError : sendError,
    failActionFunction : failActionFunction,
    uploadImage : uploadImage,
    authorizationHeaderObj : authorizationHeaderObj
}