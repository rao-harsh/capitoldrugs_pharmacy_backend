const mongoose = require('mongoose');
const Config = require('../Config');
const connectDB = () =>
{
    mongoose.connect(Config.dbConfig.config.MONGOURI,{useCreateIndex: true ,useNewUrlParser: true, useUnifiedTopology: true,useFindAndModify:false}, function(err){
        if(err)
        {
            console.log("DB error:-"+err);
            process.exit(1);
        }
        else
        {
            console.log("MongoDb connected");
        }
    });
}
module.exports = connectDB