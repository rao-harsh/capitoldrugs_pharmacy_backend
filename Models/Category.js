const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim : true
    },
    status: {
        type: Number,
        default : 0
    },
    description: {
        type: String,
        trim : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    }
},{
    timestamps : true
})

module.exports = mongoose.model('Category', CategorySchema)