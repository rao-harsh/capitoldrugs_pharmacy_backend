const mongoose = require('mongoose');

const OtpCodeSchema = new mongoose.Schema({
    otp : {
        type : String,
        trim : true,
        required : true
    },
    email : {
        type : String,
        trim : true
    },
    phone : {
        type : String,
    },
    countryCode : {
        type : String
    },
    isUsed : {
        type : Boolean
    },
},{
    timestamps : true
});
module.exports = Otpcode = mongoose.model('Otp', OtpCodeSchema);