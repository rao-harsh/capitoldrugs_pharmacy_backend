let mongoose = require('mongoose');

let UtilitySubCategorySchema = mongoose.Schema({
    title : {
        type : String,
        trim : true,
        required : true
    },
    utilitycategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'UtilityCategory',
        required : true
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    status : {
        type : Number,
        default : 0
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    }
},{
    timestamps : true
});

module.exports = mongoose.model("UtilitySubCategory",UtilitySubCategorySchema)