let mongoose = require('mongoose');

let ReviewRatingSchema = mongoose.Schema({
    item : [
        {
            productid : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Product'
            },
            review : {
                type : String
            },
            date : { type : Date },
            ratings : {
                type : Number,
                default : 0
            },
        }
    ],
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store'
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
},{
    timestamps : true
});

module.exports = mongoose.model("ReviewRatings", ReviewRatingSchema);