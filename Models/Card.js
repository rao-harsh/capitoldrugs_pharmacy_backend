let mongoose = require('mongoose');

let CardSchema = mongoose.Schema({
    cardtype : { type : String, trim : true },
    bankname : { type : String, trim : true },
    cardnumber : { type : String, trim : true },
    nameoncard : { type : String, trim : true },
    expirymonth : { type : Number},
    expiryyear : { type : Number },
    addedby : { type : mongoose.Schema.Types.ObjectId, ref : 'User' }
},{
    timestamps : true
});

module.exports = mongoose.model("Card",CardSchema)