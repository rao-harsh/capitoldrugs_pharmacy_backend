let mongoose = require('mongoose');

let DeliverChargeSchema = mongoose.Schema({
    min_amount : {
        type : Number,
        required : true
    },
    max_amount : {
        type : Number,
        required : true
    },
    deliverycharge : {
        type : Number,
        required : true
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    }
},{
    timestamps : true
});

module.exports = mongoose.model("DeliveryCharge", DeliverChargeSchema)