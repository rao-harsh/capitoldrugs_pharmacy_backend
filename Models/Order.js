let mongoose = require('mongoose');
let CONFIG = require('../Config');

let OrderSchema = mongoose.Schema({
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    order_number : { type: Number },
    item : [
        {
            productid : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Product'
            },
            utilityid : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Utilities'
            },
            extra_ingredients : [],
            quantity : {
                type : Number
            },
            discounted_price : {
                type : Number
            },
            original_price : {
                type : Number
            }
        }
    ],
    discounted_total : {
        type : Number
    },
    original_total : {
        type : Number
    },
    first_name : {
        type : String,
    },
    last_name : {
        type : String,
    },
    email : {
        type : String,
        trim : true
    },
    phone : {
        type : String,
        trim : true
    },
    address : {
        type : String,
        trim : true
    },
    address2 : {
        type : String,
        trim : true
    },
    city : {
        type : String,
        trim : true
    },
    state : {
        type : String,
        trim : true
    },
    postal_code : {
        type : String,
        trim : true
    },
    country : {
        type : String,
        trim : true
    },
    discount_code : {
        type : String,
        trim : true
    },
    payment_mode : {
        type : String
    },
    order_status : {
        type : String,
        trim : true,
        default : CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store'
    }
},{
    timestamps : true
});

module.exports = mongoose.model("Order",OrderSchema);