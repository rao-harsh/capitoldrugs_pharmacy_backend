let mongoose = require('mongoose');

let UtilityCategorySchema = mongoose.Schema({
    title : {
        type : String,
        trim : true,
        required : true
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    status : {
        type : Number,
        default : 0
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    isdeleted : {
        type : Boolean,
        default : false
    }
},{
    timestamps : true
});

module.exports = mongoose.model("UtilityCategory",UtilityCategorySchema)