let mongoose = require('mongoose');

let DiscountSchema = mongoose.Schema({
    title : {
        type : String,
        trim : true,
        required : true,
    },
    discount_code : {
        type : String,
        unique : true
    },
    measure : {
        type : String,
        trim : true
    },
    type : {
        type : String,
        trim : true
    },
    offer : {
        type : String,
        trim : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    categoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Category'
    },
    subcategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SubCategory'
    },
    productid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Product'
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    },
    expiry_date : {
        type : Date,
        required : true
    },
    status : {
        type : Number,
        default : 0
    },
    minimum_order_amount : {
        type : Number
    },
    isdeleted : {
        type : Boolean,
        default : false
    }
},{
    timestamps : true
});

module.exports = mongoose.model("Discount",DiscountSchema)