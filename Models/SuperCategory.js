const mongoose = require('mongoose');

const SuperCategorySchema = new mongoose.Schema({
    supercategoryname : {
        type : String,
        trim : true
    },
    description : {
        type : String,
        trim : true
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    status : {
        type : Number,
        default : 0
    }
},{
    timestamps : true
});

module.exports = mongoose.model('SuperCategory',SuperCategorySchema)