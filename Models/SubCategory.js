const mongoose = require('mongoose');

const SubCategorySchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim : true
    },
    description: {
        type: String,
        trim : true
    },
    status: {
        type: Number,
        default : 0
    },
    categoryid: {
        type: mongoose.Schema.Types.ObjectId,
        ref : 'Category',
        required : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    }
},{
    timestamps : true
})

module.exports = mongoose.model('SubCategory', SubCategorySchema);