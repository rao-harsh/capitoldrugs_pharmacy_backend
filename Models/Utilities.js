let mongoose = require('mongoose');

let UtilitiesSchema = mongoose.Schema({
    title : {
        type : String,
        trim : true,
        required : true
    },
    description : {
        type : String,
        trim : true
    },
    price : {
        type : Number,
        required : true
    },
    ingredients : {
        type : String
    },
    unit : {
        type : String,
        trim : true
    },
    quantity : {
        type : Number
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
    utilitycategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'UtilityCategory',
        required : true
    },
    utilitysubcategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'UtilitySubCategory'
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    status : {
        type : Number,
        default : 0
    },
    utility_image : [],
    extra_ingredients : [
        {
            name : {
                type : String,
                trim : true
            },
            quantity : {
                type : Number
            },
            price : {
                type : Number,
                required : true
            }
        }
    ]
},{
    timestamps : true
});

module.exports = mongoose.model("Utilities",UtilitiesSchema)