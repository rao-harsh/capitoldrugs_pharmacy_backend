let mongoose = require('mongoose');
const { any } = require('joi');

let ProductSchema = mongoose.Schema({
    title : {
        type : String,
        trim : true,
        required : true
    },
    description : {
        type : String,
        trim : true
    },
    discountId : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Discount'
    },
    price : {
        type : Number,
        required : true
    },
    quantity : {
        type : Number
    },
    unit : {
        type : String,
        trim : true
    },
    ingredients : {
        type : String
    },
    direction_to_use : {
        type : String
    },
    categoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Category',
        required : true
    },
    subcategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SubCategory',
        required : true
    },
    supercategoryid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    },
//     product_image :[{
//         fileName:{type:String,trim:true,sparse:true},
//         ext:{type:String,trim:true,sparse:true},
//         original:{type:String,trim:true,sparse:true},
//         thumbnail:{type:String,trim:true,sparse:true},
//         type:{type:String,trim:true,sparse:true},
//         size:{type:String,trim:true},
//    }],
    product_image:[],
    productcode : {
        type : String,
        trim : true
    },
    status : {
        type : Number,
        default : 0
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    discount_price : {
        type : Number,
    },
    expiry_date : {
        type : Date
    },
    averageratings : {
        type : Number,
        default : 0
    },
    reviewratings : [
        {
            addedby : [{
                type : mongoose.Schema.Types.ObjectId,
                ref : 'User'
            }],
            review : {
                type : String
            },
            date : { type : Date },
            ratings : {
                type : Number
            }
        }
    ],
    maximum_buyable_quantity : {
        type : String
    },
    isagerestricted : {
        type : Boolean,
        default : false
    },
    number_of_reviews : {
        type : Number,
        default : 0
    } 
},{
    timestamps : true
});

module.exports = mongoose.model("Product",ProductSchema)