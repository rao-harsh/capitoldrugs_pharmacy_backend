let mongoose = require('mongoose');

let WishListSchema = mongoose.Schema({
    addedby : {type : mongoose.Schema.Types.ObjectId, ref : 'User'},
    productid : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Product'
    }]
},{
    timestamps : true
});

module.exports = mongoose.model("Wishlist",WishListSchema);