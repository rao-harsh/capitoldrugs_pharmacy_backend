let mongoose = require('mongoose');

let StoreSchema = mongoose.Schema({
    name: {
        type: String,
        trim : true,
        required : true,
        unique : true
    },
    address: {
        type: String,
        trim : true
    },
    address2: {
        type: String,
        trim : true
    },
    city: {
        type: String,
        trim : true
    },
    state : {
        type: String,
        trim : true
    },
    postal_code: {
        type: String,
        trim : true
    },
    country: {
        type: String,
        trim : true
    },
    phone: {
        type: String,
        trim : true
    },
    email: {
        type: String,
        trim : true
    },
    status: {
        type: Number,
        default : 0
    },
    isdeleted : {
        type : Boolean,
        default : false
    },
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    supercategoryid : [{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'SuperCategory',
        required : true
    }]
},{
    timestamps : true
});

module.exports = mongoose.model("Store",StoreSchema)