
module.exports = {
    SuperCategory : require('./SuperCategory'),
    Card : require('./Card'),
    User : require('./User'),
    Otp : require('./Otp'),
    Category : require('./Category'),
    TempSubAdmin : require('./TempSubAdmin'),
    SubCategory : require('./SubCategory'),
    Store : require('./Store'),
    Product : require('./Product'),
    Discount : require('./Discount'),
    UtilityCategory : require('./UtilityCategory'),
    UtilitySubCategory : require('./UtilitySubCategory'),
    Utilities: require('./Utilities'),
    Cart : require('./Cart'),
    Order : require('./Order'),
    DeliveryCharge : require('./DeliveryCharge'),
    ReviewRatings : require('./ReviewRatings'),
    Wishlist : require('./Wishlist'),
    Payment : require('./Payment')
}