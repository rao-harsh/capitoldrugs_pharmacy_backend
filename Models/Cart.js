let mongoose = require('mongoose');

let CartSchema = mongoose.Schema({
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    item : [
        {
            productid : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Product'
            },
            utilityid : {
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Utilities'
            },
            extra_ingredients : [],
            quantity : {
                type : Number
            },
            discounted_price : {
                type : Number
            },
            original_price : {
                type : Number
            }
        }
    ],
    discounted_total : {
        type : Number
    },
    original_total : {
        type : Number
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store',
        required : true
    }
},{
    timestamps : true
});

module.exports = mongoose.model("Cart",CartSchema);