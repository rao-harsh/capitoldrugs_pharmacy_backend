let mongoose = require('mongoose');

let PaymentSchema = mongoose.Schema({
    addedby : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User',
        required : true
    },
    orderid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Order',
        required : true
    },
    card_no : {
        type : String
    },
    storeid : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Store'
    },
    mode : {
        type : String,
        required : true
    },
    amount : {
        type : Number,
        required : true
    },
    status : {
        type : String,
        default : 'pending'
    }
},{
    timestamps : true
});

module.exports = mongoose.model("Payment", PaymentSchema)