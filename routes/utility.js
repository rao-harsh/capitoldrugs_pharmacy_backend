let Controller = require('../Controllers');
let CONFIG = require('../Config');
const UniversalFunction = require('../Utils/UniversalFunction');
let Joi = require('joi');

module.exports = [
    {
        method : 'POST',
        path : '/utility/addutilitycategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.addUtilityCategory(request.payload,addedby,storeid,supercategoryid))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'add utility category',
            auth : 'UserAuth',
            validate : {
                payload : {
                    title : Joi.string().trim().required(),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/utility/getutilitycategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitycategoryid = request.query.utilitycategoryid || null
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid
                    let status = request.query.status
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.getUtilityCategory(utilitycategoryid,storeid,supercategoryid,status))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get utility category',
            validate : {
                query : {
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    utilitycategoryid : Joi.string().trim(),
                    status : Joi.number()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'PUT',
        path : '/utility/updateutilitycategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id;
                    let utilitycategoryid = request.query.utilitycategoryid;
                    let storeid = request.payload.storeid;
                    let supercategoryid = request.payload.supercategoryid;
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.updateUtilityCategory(request.payload,utilitycategoryid,addedby,storeid,supercategoryid))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'update utility category',
            auth : 'UserAuth',
            validate : {
                payload : {
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    title : Joi.string().trim()
                },
                query : {
                    utilitycategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'DELETE',
        path : '/utility/deleteutilitycategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitycategoryid = request.query.utilitycategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.deleteUtilityCategory(utilitycategoryid,addedby))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err)
                }
            },
            description : 'delete utility category',
            auth : 'UserAuth',
            validate : {
                query : {
                    utilitycategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/utility/changeutilitycategorystatus',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitycategoryid = request.query.utilitycategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.changeUtilityCategoryStatus(utilitycategoryid,addedby))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'active and deactive utility category',
            auth : 'UserAuth',
            validate : {
                query : {
                    utilitycategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/utility/addutilitysubcategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.addUtilitySubCategory(request.payload,addedby,storeid,supercategoryid))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : "add utility sub category",
            auth : 'UserAuth',
            validate : {
                payload : {
                    title : Joi.string().trim().required(),
                    utilitycategoryid : Joi.string().trim().required(),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/utility/getutilitysubcategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitysubcategoryid = request.query.utilitysubcategoryid || null
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid
                    let utilitycategoryid = request.query.utilitycategoryid || null
                    let status = request.query.status
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.getUtilitySubCategory(utilitysubcategoryid,storeid,supercategoryid,utilitycategoryid,status))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : "get utility sub category",
            validate : {
                query : {
                    utilitysubcategoryid : Joi.string().trim(),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    utilitycategoryid : Joi.string().trim(),
                    status : Joi.number()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'PUT',
        path : '/utility/updateutilitysubcategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitysubcategoryid = request.query.utilitysubcategoryid
                    let addedby = request.user.id
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.updateUtilitySubCategory(request.payload,utilitysubcategoryid,addedby,storeid,supercategoryid));
                }
                catch(err)
                {
                    return CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR
                }
            },
            description : "update utility sub category",
            auth : 'UserAuth',
            validate : {
                query : {
                    utilitysubcategoryid : Joi.string().trim().required()
                },
                payload : {
                    title : Joi.string().trim().required(),
                    utilitycategoryid : Joi.string().trim().required(),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'DELETE',
        path : '/utility/deleteutilitysubcategory',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitysubcategoryid = request.query.utilitysubcategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.deleteUtilitySubCategory(utilitysubcategoryid,addedby))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : "delete utility sub category",
            auth : 'UserAuth',
            validate : {
                query : {
                    utilitysubcategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/utility/changeutilitysubcategorystatus',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilitysubcategoryid = request.query.utilitysubcategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.changeUtilitySubCategoryStatus(utilitysubcategoryid,addedby));
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : "change utility sub category status",
            auth : 'UserAuth',
            validate : {
                query : {
                    utilitysubcategoryid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/utility/addutilities',
        config : {
            handler : async function(request,reply)
            {
                try
                {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.addUtilities(request.payload,addedby))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'add utilities',
            auth : 'UserAuth',
            validate : {
                payload : {
                    title : Joi.string().trim().required(),
                    description : Joi.string().trim().allow(''),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    price : Joi.number(),
                    unit : Joi.string().trim().required(),
                    quantity : Joi.number(),
                    utility_image : Joi.array(),
                    ingredients : Joi.string().trim().allow(),
                    utilitycategoryid : Joi.string().trim().required(),
                    utilitysubcategoryid : Joi.string().trim(),
                    extra_ingredients : Joi.array()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/utility/getutilities',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilityid  = request.query.utilityid || null
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid
                    let query = request.query
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.getUtilities(utilityid,storeid,supercategoryid,query));
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get utilities',
            validate : {
                query : {
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    utilityid : Joi.string().trim(),
                    utilitycategoryid : Joi.string().trim(),
                    utilitysubcategoryid : Joi.string().trim(),
                    status : Joi.number()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'PUT',
        path : '/utility/updateutilities',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilityid = request.query.utilityid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.updateUtilities(request.payload,utilityid,addedby))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'update utilities',
            auth : 'UserAuth',
            validate : {
                query : {
                    utilityid : Joi.string().trim().required(),
                },
                payload : {
                    title : Joi.string().trim().required(),
                    description : Joi.string().trim().allow(''),
                    storeid : Joi.string().trim().required(),
                    supercategoryid : Joi.string().trim().required(),
                    price : Joi.number(),
                    unit : Joi.string().trim().allow(''),
                    utility_image : Joi.array(),
                    quantity : Joi.number(),
                    ingredients : Joi.string().trim().allow(''),
                    utilitycategoryid : Joi.string().trim().required(),
                    utilitysubcategoryid : Joi.string().trim(),
                    extra_ingredients : Joi.array()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'DELETE',
        path : '/utility/deleteutilities',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilityid = request.query.utilityid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.deleteUtilities(utilityid,addedby));
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'delete utilities',
            auth : 'UserAuth',
            validate : {
                query : {
                    utilityid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/utility/changeutilitiesstatus',
        config : {
            handler : async function(request,reply){
                try
                {
                    let utilityid = request.query.utilityid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.utilityController.changeUtilitiesStatus(utilityid,addedby));
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'change status of utilities',
            auth : 'UserAuth',
            validate : {
                query : {
                    utilityid : Joi.string().trim().required()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    }
]