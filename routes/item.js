let Controller = require('../Controllers');
let CONFIG = require('../Config');
let Joi = require('joi');
const UniversalFunction = require('../Utils/UniversalFunction');
module.exports = [
    {
        method: 'POST',
        path: '/item/addsupercategory',
        config: {
            handler: async function (request, reply) {
                try {
                    // let storeid = request.payload.storeid
                    return Controller.itemController.addSuperCategory(request.payload);
                }
                catch (err) {
                    return CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getsupercategory',
        config: {
            handler: function (request, reply) {
                try {
                    // let storeid = request.query.storeid
                    // let supercategoryid = request.query.supercategoryid
                    return Controller.itemController.getSuperCategory()
                }
                catch (err) {
                    return CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updatesupercategory',
        config: {
            handler: function (request, reply) {
                try {
                    let supercategoryid = request.query.supercategoryid
                    // let storeid = request.payload.storeid
                    return Controller.itemController.updateSuperCategory(supercategoryid, request.payload)
                }
                catch (err) {
                    console.log(err);
                    return CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deletesupercategory',
        config: {
            handler: function (request, reply) {
                try {
                    let supercategoryid = request.query.supercategoryid
                    return Controller.itemController.deleteSuperCategory(supercategoryid);
                }
                catch (err) {
                    return CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/item/addcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let supercategoryid = request.payload.supercategoryid
                    let storeid = request.payload.storeid
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.addCategory(payload, supercategoryid, userid, storeid))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add category',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let categoryid = request.query.categoryid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getCategory(request.query.storeid, request.query.supercategoryid, categoryid, request.query.status))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get category',
            validate: {
                query: {
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim(),
                    status: Joi.number()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updatecategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let categoryid = request.query.categoryid
                    let supercategoryid = request.payload.supercategoryid
                    let user = request.user
                    let payload = request.payload
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.updateCategory(categoryid, payload, supercategoryid, user))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update category',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required()
                },
                query: {
                    categoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction

            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deletecategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let categoryid = request.query.categoryid
                    let addedBy = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.deleteCategory(categoryid, addedBy))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'delete subcategory',
            auth: 'UserAuth',
            validate: {
                query: {
                    categoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/changestatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let categoryid = request.query.categoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.changeStatus(categoryid, addedby))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'active and deactive category',
            auth: 'UserAuth',
            validate: {
                query: {
                    categoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/addsubcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let categoryid = request.payload.categoryid
                    let supercategoryid = request.payload.supercategoryid
                    let storeid = request.payload.storeid
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.addSubcategory(categoryid, supercategoryid, payload, userid, storeid))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add subcategory',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    categoryid: Joi.string().trim().required(),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getsubcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid
                    let subcategoryid = request.query.subcategoryid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getSubcategory(request.query))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get subcategory',
            validate: {
                query: {
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    subcategoryid: Joi.string().trim(),
                    categoryid: Joi.string().trim(),
                    status: Joi.number()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updatesubcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let supercategoryid = request.payload.supercategoryid
                    let categoryid = request.payload.categoryid
                    let subcategoryid = request.query.subcategoryid
                    let payload = request.payload
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.updateSubcategory(supercategoryid, categoryid, subcategoryid, payload, userid))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update sub category',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    storeid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim(),
                    supercategoryid: Joi.string().trim().required()
                },
                query: {
                    subcategoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deletesubcategory',
        config: {
            handler: async function (request, reply) {
                try {
                    let subcategoryid = request.query.subcategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.deleteSubcategory(subcategoryid, addedby));
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'delete sub category',
            auth: 'UserAuth',
            validate: {
                query: {
                    subcategoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/changesubcategorystatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let subcategoryid = request.query.subcategoryid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.changeSubCategoryStatus(subcategoryid, addedby))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'active and deactive subcategory',
            auth: 'UserAuth',
            validate: {
                query: {
                    subcategoryid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/addproduct',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let supercategoryid = request.payload.supercategoryid
                    let subcategoryid = request.payload.subcategoryid
                    let userid = request.user.id
                    let storeid = request.payload.storeid
                    let categoryid = request.payload.categoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.addProduct(supercategoryid, subcategoryid, payload, userid, storeid, categoryid));
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add product',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    quantity: Joi.number(),
                    price: Joi.number(),
                    subcategoryid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim().required(),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    unit: Joi.string().trim().allow(''),
                    ingredients: Joi.string().trim().allow(''),
                    direction_to_use: Joi.string().trim().allow(''),
                    product_image: Joi.array(),
                    isagerestricted: Joi.boolean(),
                    maximum_buyable_quantity: Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/getcategoriesproduct',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    let categoryid = request.payload.categoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getCategoriesProduct(storeid, supercategoryid, categoryid, request.payload))
                } catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get multiple categories product',
            validate: {
                payload: {
                    categoryid: Joi.array(),
                    filter: Joi.string().trim(),
                    storeid: Joi.string().trim(),
                    supercategoryid: Joi.string().trim(),
                    filter : Joi.string().trim()
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getproduct',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid
                    let categoryid = request.query.categoryid || null
                    let subcategoryid = request.query.subcategoryid || null
                    let productid = request.query.productid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getProduct(storeid, supercategoryid, categoryid, subcategoryid, productid, request.query))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get product',
            validate: {
                query: {
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim(),
                    subcategoryid: Joi.string().trim(),
                    productid: Joi.string().trim(),
                    limit: Joi.number(),
                    pageindex: Joi.number(),
                    filter: Joi.string().trim(),
                    status: Joi.number()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updateproduct',
        config: {
            handler: async function (request, reply) {
                try {
                    let supercategoryid = request.payload.supercategoryid
                    let subcategoryid = request.payload.subcategoryid
                    let productid = request.query.productid
                    let userid = request.user.id
                    let payload = request.payload
                    let storeid = request.payload.storeid
                    let categoryid = request.payload.categoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.updateProduct(subcategoryid, supercategoryid, productid, payload, userid, storeid, categoryid))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update product',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    description: Joi.string().trim().allow(''),
                    quantity: Joi.number(),
                    price: Joi.number(),
                    subcategoryid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim().required(),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    unit: Joi.string().trim().allow(''),
                    ingredients: Joi.string().trim().allow(''),
                    direction_to_use: Joi.string().trim().allow(''),
                    product_image: Joi.array(),
                    maximum_buyable_quantity: Joi.string().trim(),
                    isagerestricted: Joi.boolean()
                },
                query: {
                    productid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deleteproduct',
        config: {
            handler: async function (request, reply) {
                try {
                    let productid = request.query.productid
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.deleteProduct(productid, userid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'delete product',
            auth: 'UserAuth',
            validate: {
                query: {
                    productid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/changeproductstatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let productid = request.query.productid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.changeProductStatus(productid, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'change the status of product',
            auth: 'UserAuth',
            validate: {
                query: {
                    productid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/addstore',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.addStore(payload, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add store',
            auth: 'UserAuth',
            validate: {
                payload: {
                    name: Joi.string().trim().required(),
                    address: Joi.string().trim().allow(''),
                    address2: Joi.string().trim().allow(''),
                    city: Joi.string().trim().allow(''),
                    state: Joi.string().trim().allow(''),
                    postal_code: Joi.string().trim().allow(''),
                    country: Joi.string().trim().allow(''),
                    phone: Joi.string().trim().allow(''),
                    email: Joi.string().trim().allow(''),
                    supercategoryid: Joi.array().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getstore',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.query.storeid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getStore(storeid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get store',
            validate: {
                query: {
                    storeid: Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/item/getStoreForUser',
        config : {
            handler : async function(request,reply) {
                try {
                    let storeid = request.query.storeid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getStoreForUser(storeid));
                } catch(err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get store',
            validate: {
                query: {
                    storeid: Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updatestore',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let storeid = request.query.storeid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.updateStore(storeid, payload, addedby))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update store',
            auth: 'UserAuth',
            validate: {
                payload: {
                    name: Joi.string().trim().required(),
                    address: Joi.string().trim().allow(''),
                    address2: Joi.string().trim().allow(''),
                    city: Joi.string().trim().allow(''),
                    state: Joi.string().trim().allow(''),
                    postal_code: Joi.string().trim().allow(''),
                    country: Joi.string().trim().allow(''),
                    phone: Joi.string().trim().allow(''),
                    email: Joi.string().trim().allow(''),
                    supercategoryid: Joi.array()
                },
                query: {
                    storeid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deletestore',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.query.storeid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.deleteStore(storeid, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'delete store',
            auth: 'UserAuth',
            validate: {
                query: {
                    storeid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/changestorestatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let storeid = request.query.storeid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.changeStoreStatus(storeid, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'change store status',
            auth: 'UserAuth',
            validate: {
                query: {
                    storeid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/adddiscount',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.addDiscount(request.payload, request.query, addedby, storeid, supercategoryid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: "add discount",
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    measure: Joi.string().trim().allow(''),
                    type: Joi.string().trim().allow(''),
                    offer: Joi.string().trim().allow(''),
                    supercategoryid: Joi.string().trim().required(),
                    storeid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim().allow(''),
                    subcategoryid: Joi.string().trim().allow(''),
                    productid: Joi.string().trim().allow(''),
                    expiry_date: Joi.date(),
                    minimum_order_amount: Joi.number(),
                    description: Joi.string().trim().allow('')
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/item/getdiscount',
        config: {
            handler: async function (request, reply) {
                try {
                    let discountid = request.query.discountid || null
                    let storeid = request.query.storeid
                    let supercategoryid = request.query.supercategoryid || null
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getDiscount(discountid, storeid, supercategoryid, request.query));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get discount',
            validate: {
                query: {
                    discountid: Joi.string().trim(),
                    storeid: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim(),
                    categoryid: Joi.string().trim(),
                    subcategoryid: Joi.string().trim(),
                    productid: Joi.string().trim(),
                    type: Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/item/updatediscount',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    let storeid = request.payload.storeid
                    let supercategoryid = request.payload.supercategoryid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.updateDiscount(request.payload, request.query, addedby, storeid, supercategoryid));
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'Update discount',
            auth: 'UserAuth',
            validate: {
                payload: {
                    title: Joi.string().trim().required(),
                    measure: Joi.string().trim().allow(''),
                    type: Joi.string().trim().required(),
                    offer: Joi.string().trim().required(),
                    supercategoryid: Joi.string().trim().required(),
                    storeid: Joi.string().trim().required(),
                    categoryid: Joi.string().trim(),
                    subcategoryid: Joi.string().trim(),
                    productid: Joi.string().trim(),
                    expiry_date: Joi.date().required(),
                    minimum_order_amount: Joi.number(),
                    description: Joi.string().trim().allow('')
                },
                query: {
                    discountid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'DELETE',
        path: '/item/deletediscount',
        config: {
            handler: async function (request, reply) {
                try {
                    let discountid = request.query.discountid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.deleteDiscount(discountid, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'Delete discount',
            auth: 'UserAuth',
            validate: {
                query: {
                    discountid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/item/changediscountstatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let discountid = request.query.discountid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.changeDiscountStatus(discountid, addedby));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'Active and deactive discount',
            auth: 'UserAuth',
            validate: {
                query: {
                    discountid: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/item/getproductreview',
        config : {
            handler : async function(request,reply) {
                try {
                    let productid = request.query.productid
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.getProductReview(request.query));
                } catch(err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get product review',
            validate : {
                query : {
                    productid : Joi.string().trim()
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/item/customerreview',
        config : {
            handler : async function(request,reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.itemController.customerReview(request.query))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'product review customer wise',
            auth : 'UserAuth',
            validate : {
                query : {
                    productId : Joi.string().trim(),
                    customerId : Joi.string().trim()
                }
            }
        }
    }
]