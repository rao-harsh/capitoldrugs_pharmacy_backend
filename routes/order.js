let CONFIG = require('../Config');
let Controller = require('../Controllers');
let UniversalFunction = require('../Utils/UniversalFunction');
let Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/order/cart',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.addToCart(addedby, request.payload))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            auth: 'UserAuth',
            validate : {
                payload : {
                    productid : Joi.string().trim(),
                    quantity : Joi.number(),
                    storeid : Joi.string().trim().required(),
                    utilityid : Joi.string().trim(),
                    extra_ingredients : Joi.array()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        },
    },
    {
        method: 'GET',
        path: '/order/cart',
        config: {
            handler: async function (request, reply) {
                try {
                    let user = request.user.id
                    let storeid = request.query.storeid 
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.getCart(user,storeid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            auth: 'UserAuth',
            validate: {
                query : {
                    storeid : Joi.string().trim().required()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'PUT',
        path: '/order/cart',
        config: {
            handler: async function (request, reply) {
                try {
                    let cartid = request.query.cartid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.updateCart(cartid, addedby, request.payload))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'update cart',
            auth: 'UserAuth',
            validate : {
                payload : {
                    item : Joi.array(),
                    discounted_total : Joi.number().required(),
                    original_total : Joi.number().required(),
                    storeid : Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/order/placeorder',
        config : {
            handler : async function(request,reply)
            {
                try
                {
                    let cartid = request.payload.cartid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null,await Controller.orderController.placeOrder(cartid,addedby,request.payload))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'place order',
            auth : 'UserAuth',
            validate : {
                payload : {
                    payment_mode : Joi.string().trim(),
                    discount_code : Joi.any(),
                    cartid :  Joi.string().trim().required(),
                    storeid : Joi.string().trim().required(),
                    first_name : Joi.string().trim(),
                    last_name : Joi.string().trim(),
                    email : Joi.string().trim(),
                    phone : Joi.string().trim(),
                    address : Joi.string().trim(),
                    address2 : Joi.string().trim(),
                    city : Joi.string().trim(),
                    state : Joi.string().trim(),
                    postal_code : Joi.string().trim(),
                    country : Joi.string().trim()
                },
                failAction : UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/order/checkinventory/{cartid}',
        config: {
            handler: async function (request, reply) {
                try {
                    let cartid = request.params.cartid
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.checkStock(cartid, addedby))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err)
                }
            },
            auth: 'UserAuth',
        }
    },
    {
        method: 'POST',
        path: '/order/adddeliverycharge',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.addDeliveryCharges(addedby, request.payload))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add delivery chanrges',
            auth: 'UserAuth',
            validate  : {
                payload : {
                    min_amount : Joi.number().required(),
                    max_amount : Joi.number().required(),
                    deliverycharge : Joi.number().required()
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/order/getdeliverychargeforuser',
        config: {
            handler: async function (request, reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.getDeliveryChargeForUser(request.query))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get delivery charge'
        }
    },
    {
        method: 'PUT',
        path: '/order/deletedeliverycharge',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.deleteDeliverCharge(request.query.deliverychargeid, addedby))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'delete delivery charge',
            auth: 'UserAuth',
            validate : {
                query : {
                    deliverychargeid : Joi.string().trim().required()
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/order/getdeliverychargeforadmin',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.getDeliveryChargeForAdmin(request.query))    
                }   
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get delivery charge for admin',
            auth : 'UserAuth'
        }
    },
    {
        method: 'PUT',
        path: '/order/updatedeliverycharge',
        config: {
            handler: async function (request, reply) {
                try {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.updateDeliveryCharge(request.query.deliverychargeid, addedby, request.payload));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update delivery charge',
            auth: 'UserAuth'
        }
    },
    {
        method: 'POST',
        path: '/order/checkStock',
        config: {
            handler: async function (request, reply) {
                try {
                    let userid = request.user.id
                    console.log(request.payload);
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.stockAvailable(request.payload.cartid, userid))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'check availability of stock',
            auth: 'UserAuth',
            validate: {
                payload: {
                    cartid: Joi.string().trim().required()
                }
            }
        }
    },
    {
        method : 'POST',
        path :  '/order/verifydiscountcode',
        config : {
            handler : async function(request,reply)
            {
                try
                {
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.alreadyUsedCode(userid,request.payload))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'verfiry discount code',
            auth : 'UserAuth',
            validate : {
                payload : { 
                    discount_code : Joi.string().trim(),
                    storeid : Joi.string().trim(),
                    cartid : Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/order/addreview',
        config : {
            handler : async function(request,reply){
                try
                {
                    return await UniversalFunction.sendSuccess(null, await Controller)
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'add review',
            auth : 'UserAuth'
        }
    },
    {
        method : 'GET',
        path : '/order/myorder',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null,await Controller.orderController.myOrder(addedby,request.query.storeid, request.query.type))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get order list',
            auth : 'UserAuth',
            validate : {
                query : { 
                    storeid : Joi.string().trim(),
                    type : Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/product/reviewratings',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.addReviewRatings(request.payload,addedby))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'add review and reting on product',
            auth : 'UserAuth'
        }
    },
    {
        method : 'POST',
        path : '/order/addpayment',
        config : {
            handler : async function(request,reply){
                try
                {
                    let addedby = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.addPayment(request.payload,addedby))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);                   
                }
            },
            description : 'add payment',
            auth : 'UserAuth',
            validate : {
                payload : {
                    orderid : Joi.string().trim().required(),
                    mode : Joi.string().trim().required(),
                    amount : Joi.number().required(),
                    storeid : Joi.string().trim().required(),
                    status : Joi.string().trim(),
                    card_no : Joi.number()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'GET',
        path : '/order/getpayment',
        config : {
            handler : async function(request,reply){
                try
                {
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.getPayment(request.query));
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err)
                }
            },

        }
    },
    {
        method : 'POST',
        path : '/order/changeorderstatus',
        config : {
            handler : async function(request,reply){
                try
                {
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.changeOrderStatus(request.payload))
                }
                catch(err)
                {
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'change order status',
            auth : 'UserAuth',
            validate : {
                payload : {
                    storeid : Joi.string().trim().required(),
                    orderid : Joi.string().trim().required(),
                    order_status : Joi.string().trim().required()
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/order/getorder',
        config : {
            handler : async function(request,reply){
                try
                {
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.getOrder(request.query))
                }
                catch(err)
                {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            }
        }
    },
    {
        method : 'POST',
        path : '/order/cancelorder',
        config : {
            handler : async function(request,reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.orderController.cancelOrder(request.payload))
                } catch(err){
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            auth : 'UserAuth',
            validate : {
                payload : {
                    orderid : Joi.string().trim()
                }
            }
        }
    },
    {
        method : 'POST',
        path : '/admin/review',
        config : {
            handler : async function(request,reply) {
                try {

                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            }
        }
    }
]