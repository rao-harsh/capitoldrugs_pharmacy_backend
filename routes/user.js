let Controller = require('../Controllers');
let CONFIG = require('../Config');
let Joi = require('joi');
const UniversalFunction = require('../Utils/UniversalFunction');

module.exports = [
    {
        method: "POST",
        path: '/login',
        config: {
            handler: async function (request, reply) {
                try {
                    const payload = request.payload
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.loginAdmin(request.payload));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'login user',
            validate: {
                payload: {
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: "POST",
        path: "/admin/addsubadmin",
        config: {
            handler: async function (request, reply) {
                try {
                    const payload = request.payload
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.addSubAdmin(payload));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'add subadmin',
            auth: 'UserAuth',
            validate: {
                payload: {
                    email: Joi.string().trim().required()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: "POST",
        path: "/register",
        config: {
            handler: async function (request, reply) {
                try {
                    const payload = request.payload
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.customerRegister(payload));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err)
                }
            },
            description: 'registration of user',
            validate: {
                payload: {
                    first_name: Joi.string().trim().allow(''),
                    last_name: Joi.string().trim().allow(''),
                    email: Joi.string().trim().allow(''),
                    phone: Joi.number().allow(''),
                    password: Joi.string().trim().allow(''),
                    address: Joi.array(),
                    // address: Joi.string().trim().allow(''),
                    // address2: Joi.string().trim().allow(''),
                    // city: Joi.string().trim().allow(''),
                    // state: Joi.string().trim().allow(''),
                    // postal_code: Joi.string().trim().allow(''),
                    // country: Joi.string().trim().allow('')
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: "POST",
        path: "/requestotp",
        config: {
            handler: async function (request, reply) {
                try {
                    const email = request.payload
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.verifyEmail(email, "forgotpassword"));
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'request for otp',
            validate: {
                payload: {
                    email: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/forgotpassword',
        config: {
            handler: async function (request, reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.forgotPassword(request.payload));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'forgot password',
            validate: {
                payload: {
                    email: Joi.string().trim().required(),
                    password: Joi.string().trim().required()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: "/getuserprofile",
        config: {
            handler: async function (request, reply) {
                try {
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getUserProfile(userid))
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'shows user profile',
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: "POST",
        path: "/validateotp",
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let otp = request.payload.otp
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.validateOtp(payload, otp))
                }
                catch (err) {
                    console.log(err)
                    return await UniversalFunction.sendError(err)
                }
            },
            description: 'verify the otp',
            validate: {
                payload: {
                    email: Joi.string().trim(),
                    otp: Joi.string().trim().required(),
                    phone: Joi.string(),
                    countryCode: Joi.string()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getsubadmin',
        config: {
            handler: async function (request, reply) {
                try {
                    let subadminid = request.query.subadminid
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getSubAdmin(subadminid))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get sub admin',
            auth: 'UserAuth',
            validate: {
                query: {
                    subadminid: Joi.string().trim()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: "POST",
        path: "/changepassword",
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let userid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.changePassword(payload, userid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'change password',
            auth: 'UserAuth',
            validate: {
                payload: {
                    old_password: Joi.string().trim().required(),
                    password: Joi.string().trim().required()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }

        }
    },
    {
        method: 'POST',
        path: '/updateuserprofile',
        config: {
            handler: async function (request, reply) {
                try {
                    let payload = request.payload
                    let profileid = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.updateUserProfile(payload, profileid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'update user profile',
            auth: 'UserAuth',
            validate: {
                payload: {
                    first_name: Joi.string().trim().allow(''),
                    last_name: Joi.string().trim().allow(''),
                    email: Joi.string().trim().allow(''),
                    phone: Joi.string().allow(''),
                    password: Joi.string().trim().allow(''),
                    // address: Joi.string().trim().allow(''),
                    // address2: Joi.string().trim().allow(''),
                    address : Joi.array(),
                    city: Joi.string().trim().allow(''),
                    state: Joi.string().trim().allow(''),
                    postal_code: Joi.string().trim().allow(''),
                    profile_image: Joi.string().trim(),
                    country: Joi.string().trim().allow('')
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/changesubadminstatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let subadminid = request.query.subadminid
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.changeSubadminStatus(subadminid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'active or deactive sub admin',
            auth: 'UserAuth',
            validate: {
                query: {
                    subadminid: Joi.string().trim()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/admin/getcustomer',
        config: {
            handler: async function (request, reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getAllCustomer());
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get all customer',
            auth: 'UserAuth',
            validate: {
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/admin/changecustomerstatus',
        config: {
            handler: async function (request, reply) {
                try {
                    let customerid = request.query.customerid
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.changeCustomerStatus(customerid));
                }
                catch (err) {
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'active or deactive customer',
            auth: 'UserAuth',
            validate: {
                query: {
                    customerid: Joi.string().trim()
                },
                headers: UniversalFunction.authorizationHeaderObj,
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'POST',
        path: '/user/requestotpforregister',
        config: {
            handler: async function (request, reply) {
                try {
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.verifyEmail(request.payload, "register"))
                }
                catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'request for otp either by using email or phone',
            validate: {
                payload: {
                    email: Joi.string().lowercase().when('phone', { is: Joi.exist(), then: Joi.optional(), otherwise: Joi.required() }),
                    phone: Joi.string(),
                    countryCode: Joi.string().when('phone', { is: Joi.exist(), then: Joi.required(), otherwise: Joi.optional() })
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method: 'GET',
        path: '/api/dashboardData',
        config: {
            handler: async function (request, reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getDashboardData(request.query,user))
                } catch (err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description: 'get dashboard graph report',
            auth : 'UserAuth',
            validate: {
                query : {
                    startDate: Joi.date(),
                    endDate: Joi.date(),
                    storeid: Joi.string().trim()
                },
                failAction: UniversalFunction.failActionFunction
            }
        }
    },
    {
        method : 'POST',
        path : '/api/wishlist',
        config : {
            handler : async function (request,reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.addWishlist(request.payload,user))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'add product to wishlist',
            auth : 'UserAuth',
            validate : {
                payload : {
                    productid : Joi.string().trim()
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/api/wishlist',
        config : {
            handler : async function (request,reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getWishlist(user))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get wishlist',
            auth : 'UserAuth',
        }
    },
    {
        method : 'DELETE',
        path : '/api/wishlist',
        config : {
            handler : async function (request,reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.deleteWishlist(request.query,user))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err);
                }
            },
            description : 'get wishlist',
            auth : 'UserAuth',
            validate : {
                query : {
                    productid : Joi.string().trim()
                }
            }
        }
    },
    {
        method : 'POST',
        path : '/api/card',
        config : {
            handler : async function (request,reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.addCard(request.payload,user))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err); 
                }
            },
            description : 'add payment card',
            auth : 'UserAuth',
            validate : {
                payload : {
                    cardtype : Joi.string().trim(),
                    bankname : Joi.string().trim(),
                    cardnumber : Joi.string().trim(),
                    nameoncard : Joi.string().trim(),
                    expirymonth : Joi.number(),
                    expiryyear : Joi.number()
                }
            }
        }
    },
    {
        method : 'GET',
        path : '/api/card',
        config : {
            handler : async function (request,reply) {
                try {
                    let user = request.user.id
                    return await UniversalFunction.sendSuccess(null, await Controller.userController.getCard(user))
                } catch(err) {
                    console.log(err);
                    return await UniversalFunction.sendError(err); 
                }
            },
            description : 'get payment cards',
            auth : 'UserAuth'
        }
    }
]