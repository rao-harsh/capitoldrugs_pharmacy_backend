


let findOne = function(model,query,projection,options){
    return new Promise((resolve,reject)=> {
        model.findOne(query,projection,options,function(err,data) {
            if(err) reject(err)
            resolve(data)
        });
    });
}

let findOneAndUpdate = function(model,condition,update,options){
    return new Promise((resolve,reject)=> {
        model.findOneAndUpdate(condition,update,options, function(err,result){
            if(err) reject(err)
            else resolve(result)
        });
    });
}


let savedata = function(model,data){
    return new Promise((resolve,reject) => {
        model(data).save((err,result) => {
            if (err) reject(err)
            else resolve(result)
        });
    });
}


let find = function(model,condition,projection,options) {
    return new Promise((resolve,reject)=> {
        model.find(condition,projection,options,function(err,result){
            if(err) reject(err)
            else resolve(result)
        })
    })
}

let remove = function (model, condition) {
    return new Promise((resolve, reject) => {
        model.findOneAndRemove(condition, function (err, result) {
            if (err) reject(err);
            else resolve(result)
        });
    })
};

let deleteMany = function(model,condition){
    return new Promise((resolve,reject) => {
        model.deleteMany(condition,function(err,result) {
            if(err) reject(err);
            else resolve(result)
        })
    })
}
let populateData = function(model,query,projection,options,collectionsOptions) {
    return new Promise((resolve,reject) => {
        model.find(query,projection,options).populate(collectionsOptions).exec(function(err,result){
            if(err) reject(err);
            else resolve(result)
        });
    });
}
let update = function (model, conditions, update, options) {
    return new Promise((resolve, reject) => {
        model.updateMany(conditions, update, options, function (err, result) {
            if (err) reject(err);
            else resolve(result)
        });
    });
};

let findOnePopulateData = function (model, query, projection, options, collectionOptions) {
    return new Promise((resolve, reject) => {
        model.findOne(query, projection, options).populate(collectionOptions).exec(function (err, result) {
            if (err) reject(err);
            else resolve(result)
        });
    });
}

let aggregateData = function (model, group) {
    return new Promise((resolve, reject) => {
        model.aggregate(group, function (err, result) {
            if (err) reject(err);
            else resolve(result)
        });
    })
};

let count = function (model, condition) {
    return new Promise((resolve, reject) => {
        model.countDocuments(condition, function (err, result) {
            if (err) reject(err);
            else resolve(result)
        })
    })
};

module.exports = {
    findOne : findOne,
    findOneAndUpdate : findOneAndUpdate,
    savedata : savedata,
    find : find,
    remove : remove,
    populateData : populateData,
    findOnePopulateData : findOnePopulateData,
    deleteMany : deleteMany,
    update : update,
    aggregateData : aggregateData,
    count : count
}