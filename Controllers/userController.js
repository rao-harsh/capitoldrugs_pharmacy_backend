let CONFIG = require('../Config');
let Service = require('../Services');
let Model = require('../Models');
let generator = require('generate-password');
let sms = require('../Lib/sms');
let UniversalFunction = require('../Utils/UniversalFunction');
let bcrypt = require('bcryptjs');
let Lib = require('../Lib/email');
const TempSubAdmin = require('../Models/TempSubAdmin');
const moment = require('moment');
const { custom } = require('joi');
let mongoose = require('mongoose');
async function addSuperCategory(superCategoryData) {
    try {
        let dataToSave = {
            supercategoryname: superCategoryData.supercategoryname
        }
        await Service.queries.savedata(Model.SuperCategory, dataToSave);
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_ADDED_SUCCESS
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_ADDED_FAILED)
    }
}

async function getSuperCategory() {
    try {
        let superCategory = await Service.queries.find(Model.SuperCategory, {}, {}, {});
        let successMsg = CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: superCategory
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_GET_FAILED)
    }
}

async function verifyEmail(payload, forWhat) {
    try {
        let email = payload.email || null
        let phone = payload.phone || null
        if (forWhat == "register") {
            if (email !== null) {
                let user = await Service.queries.findOne(Model.User, { email: payload.email }, {}, {});
                if (user) {
                    return Promise.reject(CONFIG.appConstants.STATUS_MSG.EMAIL_ALREADY_EXIST)
                }
                else {
                    return sendEmailOtp(payload.email);
                }
            }
            else if (phone !== null) {
                let user = await Service.queries.findOne(Model.User, { phone: payload.phone }, {}, {});
                if (user) {
                    return Promise.reject(CONFIG.appConstants.STATUS_MSG.PHONE_ALREADY_EXIST)
                }
                else {
                    return sendPhoneOtp(payload)
                }
            }
        }
        else if (forWhat == "forgotpassword") {
            let user = await Service.queries.findOne(Model.User, { email: email }, {}, {});
            if (user) {
                console.log(user);
                return sendEmailOtp(user.email);
            }
            else {
                return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_EMAIL)
            }
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.EMAIL_VARIFY_FAILED)
    }
}

async function sendPhoneOtp(payload) {
    let otp = await UniversalFunction.generateOtp();
    let number = payload.countryCode + '' + payload.phone;
    let content = `Your verification code is ${otp}`
    await sms.sendSms(number, content);
    return UniversalFunction.storeOtp(otp, payload)
}
async function sendEmailOtp(email) {
    try {
        let otp = await UniversalFunction.generateOtp();
        let to = email
        let subject = "Capitol Drug verification code"
        let content = `Your verification code is ${otp}`
        await Lib.sendEmail(to, subject, content, null);
        return UniversalFunction.storeOtp(otp, to)
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SEND_OTP_FAILED)
    }
}

async function validateOtp(payload, otp) {
    console.log(payload, 'payload in validate otp')
    let otpData = ""
    let email = payload.email
    let phone = payload.phone
    if (email !== null) {
        otpData = await Service.queries.findOne(Model.Otp, { email: payload.email }, {}, { sort: { _id: -1 } });
    }
    else if (phone !== null) {
        otpData = await Service.queries.findOne(Model.Otp, { phone: payload.phone, countryCode: payload.countryCode }, {}, { sort: { _id: -1 } });
    }
    let dbDate = new Date(otpData.updatedAt);
    let cDate = new Date();
    console.log(otpData);
    if (otpData.otp == otp) {
        if (cDate - dbDate < 300000) {
            //const updateOtpData = await Model.Otpcode.findOneAndUpdate({ email : email }, {$set : { isUsed: true }}).sort({ _id: -1 });
            if (payload.email) {
                await Service.queries.findOneAndUpdate(Model.Otp, { email: payload.email }, { isUsed: true }, { sort: { _id: -1 } })
                return CONFIG.appConstants.STATUS_MSG.VALIDATE_OTP
            }
            else if (payload.phone) {
                await Service.queries.findOneAndUpdate(Model.Otp, { phone: payload.phone, countryCode: payload.countryCode }, { isUsed: true }, { sort: { _id: -1 } })
                return CONFIG.appConstants.STATUS_MSG.VALIDATE_OTP
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.OTP_EXPIRED)
        }
    }
    else {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_OTP)
    }
}

async function updateSuperCategory(id, superCategoryData) {
    try {
        let dataToUpdate = {
            supercategoryname: superCategoryData.supercategoryname,
            isactive: superCategoryData.isActive || true
        }
        await Service.queries.findOneAndUpdate(Model.SuperCategory, { _id: id }, dataToUpdate, {});
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_UPDATED_SUCCESS
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_UPDATED_FAILED)
    }
}

async function deleteSuperCategory(id) {
    try {
        let dataToUpdate = {
            isdelete: true
        }
        await Service.queries.findOneAndUpdate(Model.SuperCategory, { _id: id }, dataToUpdate, {});
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_DELETED_SUCCESS
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_DELETED_FAILED)
    }
}


async function addSubAdmin(subAdminData) {
    try {
        let exist = await Service.queries.findOne(Model.User, { email: subAdminData.email }, {}, {});
        if (exist) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.EMAIL_ALREADY_EXIST)
        }
        let password = await generatePassword();
        let content = `<h2>Now, you are able to access your account.Please verify your password:-${password}</h2>`
        password = await UniversalFunction.encryptPassword(password)
        await Lib.sendEmail(subAdminData.email, "Verify your account", content)
        let dataToSave = {
            email: subAdminData.email,
            password: password
        }
        await Service.queries.savedata(Model.TempSubAdmin, dataToSave)
        return CONFIG.appConstants.STATUS_MSG.AUTOGENERATED_PASSWORD_SENT_SUCCESS
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUBADMIN_ADDED_FAILED)
    }
}

async function generatePassword() {
    let password = generator.generate({
        length: 10,
        numbers: true
    });
    return password
}

async function loginAdmin(loginData) {
    try {
        let tempsubadmin = await Service.queries.findOne(Model.TempSubAdmin, { email: loginData.email }, {}, { sort: { _id: -1 } });
        console.log(tempsubadmin);
        if (tempsubadmin) {
            // first time login
            if (tempsubadmin.isverified == false && tempsubadmin.isautogenerated == true) {
                let isMatch = await bcrypt.compare(loginData.password, tempsubadmin.password);
                console.log(isMatch);
                if (isMatch) {
                    let tempsubadmintoken = await UniversalFunction.generateToken(tempsubadmin._id, "tempsubadmin");
                    let dataToUpdate = {
                        isverified: true,
                        token: tempsubadmintoken
                    }
                    await Service.queries.findOneAndUpdate(Model.TempSubAdmin, { email: loginData.email }, dataToUpdate, { sort: { _id: -1 } });
                    let successMsg = CONFIG.appConstants.STATUS_MSG.PASSWORD_VALIDATE_SUCCESS
                    const responseData = {
                        statusCode: successMsg.statusCode,
                        customMessage: successMsg.customMessage,
                        isfirsttime: true,
                        token: tempsubadmintoken
                    }
                    return responseData
                }
                else {
                    return Promise.reject(CONFIG.appConstants.STATUS_MSG.AUTOGENERATED_INVALID_PASSWORD)
                }
            }
            // first time validate complete but password is auto generated
            if (tempsubadmin.isverified == true && tempsubadmin.isautogenerated == true) {
                let successMsg = CONFIG.appConstants.STATUS_MSG.REQUEST_CHANGE_PASSWORD
                let responseData = {
                    statusCode: successMsg.statusCode,
                    customMessage: successMsg.customMessage,
                    isfirsttime: true,
                    token: tempsubadmin.token
                }
                return responseData
            }
        }
        else {
            let user = await Service.queries.findOne(Model.User, { email: loginData.email }, {}, {});
            console.log(user)
            if (user) {
                let isMatch = await bcrypt.compare(loginData.password, user.password)
                console.log(isMatch);
                if (isMatch) {
                    if (user.status == 1) {
                        return Promise.reject(CONFIG.appConstants.STATUS_MSG.USER_BLOCK)
                    }
                    else {
                        let token = await UniversalFunction.generateToken(user.id, user.type);
                        let dataToUpdate = {
                            token: token
                        }
                        user = await Service.queries.findOneAndUpdate(Model.User, { email: loginData.email }, dataToUpdate, {});
                        let store
                        if (user.type == CONFIG.appConstants.SERVICE.USER_TYPE.SUBADMIN) {
                            store = await Service.queries.findOne(Model.Store, { addedby: user._id }, {}, {});
                            if (!store) store = null
                        }
                        const successMsg = CONFIG.appConstants.STATUS_MSG.LOGIN_SUCCESS
                        let responseData = ""
                        if (user.type == CONFIG.appConstants.SERVICE.USER_TYPE.SUBADMIN) {
                            responseData = {
                                statusCode: successMsg.statusCode,
                                customMessage: successMsg.customMessage,
                                token: token,
                                type: user.type,
                                store: store
                            }
                        }
                        else {
                            responseData = {
                                statusCode: successMsg.statusCode,
                                customMessage: successMsg.customMessage,
                                token: token,
                                type: user.type,
                            }
                        }
                        return responseData
                    }
                }
                else {
                    console.log("invalid ")
                    return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_LOGINDATA)
                }
            }
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_LOGINDATA)
        }
    }
    catch (err) {
        console.log(err);
        return "error occured"
    }
}


async function changePassword(data, userid) {
    try {
        let tempsubadmin = await Service.queries.findOne(Model.TempSubAdmin, { _id: userid }, {}, { sort: { _id: -1 } });
        if (tempsubadmin) {
            let email = tempsubadmin.email
            let query = {
                isverified: true,
                isautogenerated: true,
                email: email
            }
            tempsubadmin = await Service.queries.findOne(Model.TempSubAdmin, query, {}, { sort: { _id: -1 } });
            console.log(tempsubadmin);
            if (tempsubadmin) {
                let password = await UniversalFunction.encryptPassword(data.password)
                let token = await UniversalFunction.generateToken(tempsubadmin.id, "subadmin")
                let dataToUpdate = {
                    isautogenerated: false,
                }
                await Service.queries.findOneAndUpdate(Model.TempSubAdmin, { email: email }, dataToUpdate, { sort: { _id: -1 } });
                let dataToSave = {
                    email: email,
                    password: password,
                    token: token,
                    status: 0,
                    type: 'subadmin'
                }
                await Service.queries.savedata(Model.User, dataToSave);
                await Service.queries.deleteMany(Model.TempSubAdmin, { email: email }, {}, {});
                let successMsg = CONFIG.appConstants.STATUS_MSG.PASSWORD_CHANGE_SUCCESS
                let responseData = {
                    statusCode: successMsg.statusCode,
                    customMessage: successMsg.customMessage,
                    token: token
                }
                return responseData
            }
        }
        let user = await Service.queries.findOne(Model.User, { _id: userid }, {}, {});
        if (user) {
            if (data.old_password == null) {
                return CONFIG.appConstants.STATUS_MSG.OLD_PASSWORD_NULL
            }
            let isMatch = await bcrypt.compare(data.old_password, user.password)
            console.log(isMatch);
            if (isMatch) {
                let store = {};
                let password = await UniversalFunction.encryptPassword(data.password)
                let token = await UniversalFunction.generateToken(user.id, user.type) 
                let dataToUpdate = {
                    password: password,
                    token: token,
                    isautogenerated: false
                }
                await Service.queries.findOneAndUpdate(Model.User, { email: user.email }, dataToUpdate, {});
                let successMsg = CONFIG.appConstants.STATUS_MSG.PASSWORD_CHANGE_SUCCESS
                let responseData = {
                    statusCode : successMsg.statusCode,
                    customMessage : successMsg.customMessage,
                    type : user.type
                }
                if(user.storeid) {
                    store = await Service.queries.findOne(Model.Store,{ _id : user.storeid });
                    responseData.store = store
                } else { responseData.store = null }
                return responseData
            }
            else {
                return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_OLD_PASSWORD)
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_EMAIL)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.PASSWORD_CHANGE_FAILED)
    }
}

async function getUserProfile(userid) {
    console.log(userid)
    let user = await Service.queries.findOne(Model.User, { _id: userid }, {}, { select: "-password" });

    console.log(user)
    if (user) {
        let successMsg = CONFIG.appConstants.STATUS_MSG.USER_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: user
        }
        return responseData
    }
    else {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.USER_GET_FAILED)
    }
}

async function customerRegister(customerData) {
    try {
        let customer = await Service.queries.findOne(Model.User, { email: customerData.email }, {}, {});
        if (customer) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.EMAIL_ALREADY_EXIST)
        }
        let password = await UniversalFunction.encryptPassword(customerData.password);
        let dataToSave = {
            first_name: customerData.first_name,
            last_name: customerData.last_name,
            email: customerData.email,
            phone: customerData.phone,
            password: password,
            address: customerData.address,
            // address2: customerData.address2,
            // city: customerData.city,
            // state: customerData.state,
            // postal_code: customerData.postal_code,
            // country: customerData.country,
            profile_image: customerData.profile_image,
            type: 'customer',
            status: 0
        }
        user = await Service.queries.savedata(Model.User, dataToSave);
        let token = await UniversalFunction.generateToken(user._id, "customer");
        await Service.queries.findOneAndUpdate(Model.User, { _id: user._id }, { token: token }, {});
        let successMsg = CONFIG.appConstants.STATUS_MSG.CUSTOMER_REGISTER_SUCCESS
        const responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: token
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CUSTOMER_REGISTER_FAILED)
    }
}

async function forgotPassword(data) {
    try {
        let user = await Model.User.findOne({ email: data.email })
        let password = await UniversalFunction.encryptPassword(data.password)
        let token = await UniversalFunction.generateToken(user.id, user.type)
        user.password = password
        user.token = token
        let store
        if (user.type == CONFIG.appConstants.SERVICE.USER_TYPE.SUBADMIN) {
            store = await Service.queries.findOne(Model.Store, { addedby: user._id }, {}, {});
            if (!store) store = null
        }
        await user.save();
        let successMsg = CONFIG.appConstants.STATUS_MSG.PASSWORD_CHANGE_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            type : user.type,
            token: token,
            store: store
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.PASSWORD_CHANGE_FAILED)
    }
}

async function getSubAdmin(subadminid) {
    try {
        let subadmin
        if (subadminid) {
            let query = {
                type: 'subadmin',
                _id: subadminid,
                isdeleted: false
            }
            subadmin = await Service.queries.findOnePopulateData(Model.User, query, {}, {}, "storeid");
        }
        else {
            let query = {
                type: 'subadmin',
                isdeleted: false
            }
            subadmin = await Service.queries.populateData(Model.User, query, {}, { sort: { _id: -1 } }, "storeid")
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.GET_SUBADMIN_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: subadmin
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.GET_SUBADMIN_FAILED)
    }
}

async function changeSubadminStatus(subadminid) {
    try {
        let subadmin = await Service.queries.findOne(Model.User, { _id: subadminid }, {}, {});
        if (subadmin.status == 0) {
            await Service.queries.findOneAndUpdate(Model.User, { _id: subadminid }, { status: 1 }, {});
        }
        else {
            await Service.queries.findOneAndUpdate(Model.User, { _id: subadminid }, { status: 0 }, {});
        }
        return CONFIG.appConstants.STATUS_MSG.CHANGE_SUBADMIN_STATUS_SUCCESS
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CHANGE_SUBADMIN_STATUS_FAILED)
    }
}
async function updateUserProfile(profileData, profileid) {
    try {
        let dataToUpdate = {
            first_name: profileData.first_name,
            last_name: profileData.last_name,
            email: profileData.email,
            phone: profileData.phone,
            address: profileData.address,
            // address2: profileData.address2,
            city: profileData.city,
            state: profileData.state,
            postal_code: profileData.postal_code,
            country: profileData.country,
            profile_image: profileData.profile_image,
        }
        let profile = await Service.queries.findOneAndUpdate(Model.User, { _id: profileid }, dataToUpdate, {});
        if (profile !== null) {
            return CONFIG.appConstants.STATUS_MSG.UPDATE_USER_PROFILE_SUCCESS
        }
        else {
            return CONFIG.appConstants.STATUS_MSG.UPDATE_USER_PROFILE_FAILED
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UPDATE_USER_PROFILE_FAILED)
    }
}

async function getAllCustomer() {
    try {
        let query = {
            type: 'customer',
            isdeleted: false
        }
        let customer = await Service.queries.find(Model.User, { type: 'customer' }, {}, { sort: { _id: -1 } });
        let successMsg = CONFIG.appConstants.STATUS_MSG.CUSTOMER_FETCH_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: customer
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CUSTOMER_FETCH_FAILED)
    }
}

async function changeCustomerStatus(customerid) {
    try {
        let query = {
            _id: customerid,
            type: 'customer',
            isdeleted: false
        }
        let customer = await Service.queries.findOne(Model.User, query, {}, {});
        console.log(customer);
        if (customer.status == 0) {
            customer = await Service.queries.findOneAndUpdate(Model.User, query, { status: 1 }, {});
        }
        if (customer.status == 1) {
            customer = await Service.queries.findOneAndUpdate(Model.User, query, { status: 0 }, {});
        }
        return CONFIG.appConstants.STATUS_MSG.CHANGE_CUSTOMER_STATUS_SUCCESS
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CHANGE_CUSTOMER_STATUS_FAILED)
    }
}

async function getDashboardData(query, user) {
    try {
        let startDate = query.startDate || null
        let endDate = query.endDate || null
        let storeid = query.storeid || null
        let salesQueryData = {};
        let ordersQueryData = {};
        if (startDate !== null && endDate !== null) {
            salesQueryData = { updatedAt: { $lte: endDate, $gte: startDate }, $or: [{ order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED }, { order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED }], storeid: mongoose.Types.ObjectId(storeid) }
            ordersQueryData = { updatedAt: { $lte: endDate, $gte: startDate }, order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING, storeid: mongoose.Types.ObjectId(storeid) }
        } else {
            let date = new Date
            let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            let cdate = new Date();
            salesQueryData = { updatedAt: { $lte: cdate, $gte: firstDay }, $or: [{ order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED }, { order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED }], storeid: mongoose.Types.ObjectId(storeid) }
            ordersQueryData = { updatedAt: { $lte: cdate, $gte: firstDay }, order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING, storeid: mongoose.Types.ObjectId(storeid) }
        }
        let salesGroup = [
            { $match: salesQueryData },
            { $project: { _id: 1, year: { $year: "$updatedAt" }, month: { $month: "$updatedAt" }, day: { $dayOfMonth: "$updatedAt" }, discounted_total: 1 } },
            { $group: { _id: { year: "$year", month: "$month", day: "$day" }, totalSale: { $sum: "$discounted_total" }, data: { $push: "$$ROOT" } } },
            { $sort: { _id: -1 } }
        ]
        let sales = await Service.queries.aggregateData(Model.Order, salesGroup);
        let ordersGroup = [
            { $match: ordersQueryData },
            { $project: { _id: 1, year: { $year: "$updatedAt" }, month: { $month: "$updatedAt" }, day: { $dayOfMonth: "$updatedAt" }, discounted_total: 1 } },
            { $group: { _id: { year: "$year", month: "$month", day: "$day" }, totalSale: { $sum: "$discounted_total" }, data: { $push: "$$ROOT" } } },
            { $sort: { _id: -1 } }
        ]
        let orders = await Service.queries.aggregateData(Model.Order, ordersGroup);
        let users = await Service.queries.count(Model.User, { type: "customer" });
        let products = await Service.queries.count(Model.Product, { storeid: storeid });
        let totalOrders = await Service.queries.count(Model.Order, { storeid: storeid, $or: [{ order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED }, { order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED, order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING }] });
        let totalSales = await Service.queries.find(Model.Order, { storeid: storeid, $or: [{ order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED }, { order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED }] }, {}, {});
        let totalSale = 0;
        totalSales.map((order) => { totalSale = totalSale + order.discounted_total });
        let responseData = {
            sales: sales,
            orders: orders,
            totalUsers: users,
            totalProducts: products,
            totalOrders: totalOrders,
            totalSales: totalSale
        }
        return responseData
    } catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function addWishlist(payload, userid) {
    try {
        payload.addedby = userid
        if (await Service.queries.findOne(Model.Wishlist, { addedby: userid })) {
            console.log("if condition");
            if(await Service.queries.findOne(Model.Wishlist,{ addedby : userid, productid : payload.productid },{})) {
                return Promise.reject("Product already into your wishlist");
            }
            let wishlist = await Service.queries.findOneAndUpdate(Model.Wishlist,{ addedby : userid },{ $push : { productid : payload.productid } },{});
            let successMsg = CONFIG.appConstants.STATUS_MSG.ADD_WISHLIST
            if (wishlist !== null) {
                let responseData = {
                    statusCode: successMsg.statusCode,
                    customMessage: successMsg.customMessage,
                }
                return responseData
            }
        } else {
            let wishlist = await Service.queries.savedata(Model.Wishlist, payload);
            let successMsg = CONFIG.appConstants.STATUS_MSG.ADD_WISHLIST
            if (wishlist !== null) {
                let responseData = {
                    statusCode: successMsg.statusCode,
                    customMessage: successMsg.customMessage,
                }
                return responseData
            }
        }
    } catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getWishlist(userid) {
    try {
        let wishlist = await Service.queries.findOnePopulateData(Model.Wishlist,{ addedby : userid },{},{},"productid");
        let successMsg = CONFIG.appConstants.STATUS_MSG.GET_WISHLIST
        let responseData = {
            statusCode : successMsg.statusCode,
            customMessage : successMsg.customMessage,
            data : wishlist
        }
        return responseData
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function deleteWishlist(query,userid) {
    try {
        let wishlist = await Service.queries.findOneAndUpdate(Model.Wishlist,{ addedby : userid },{ $pull : { productid : query.productid } },{});
        if(wishlist !==null) {
            let successMsg = CONFIG.appConstants.STATUS_MSG.DELETE_WISHLIST
            let responseData = {
                statusCode : successMsg.statusCode,
                customMessage : successMsg.customMessage
            }
            return responseData
        }
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function addCard(payload,userid) {
    try {
        payload.addedby = userid
        let card = await Service.queries.savedata(Model.Card,payload);
        if(card!==null) {
            let successMsg = CONFIG.appConstants.STATUS_MSG.ADD_CARD
            let responseData = {
                statusCode : successMsg.statusCode,
                customMessage : successMsg.customMessage
            }
            return responseData
        }
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}
async function getCard(userid) {
    try {
        let card = await Service.queries.find(Model.Card,{ addedby : userid },{},{ sort : { _id : -1  }});
        let successMsg = CONFIG.appConstants.STATUS_MSG.GET_CARD 
        let responseData = {
            statusCode : successMsg.statusCode,
            customMessage : successMsg.customMessage,
            data : card
        }
        return responseData
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}
module.exports = {
    addCard : addCard,
    getCard : getCard,
    addWishlist: addWishlist,
    getWishlist : getWishlist,
    deleteWishlist : deleteWishlist,
    addSuperCategory: addSuperCategory,
    getSuperCategory: getSuperCategory,
    updateSuperCategory: updateSuperCategory,
    deleteSuperCategory: deleteSuperCategory,
    addSubAdmin: addSubAdmin,
    loginAdmin: loginAdmin,
    changePassword: changePassword,
    verifyEmail: verifyEmail,
    validateOtp: validateOtp,
    customerRegister: customerRegister,
    getUserProfile: getUserProfile,
    forgotPassword: forgotPassword,
    getSubAdmin: getSubAdmin,
    updateUserProfile: updateUserProfile,
    changeSubadminStatus: changeSubadminStatus,
    getAllCustomer: getAllCustomer,
    changeCustomerStatus: changeCustomerStatus,
    getDashboardData: getDashboardData
}

