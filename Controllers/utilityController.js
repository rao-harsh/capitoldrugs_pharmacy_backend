let CONFIG = require('../Config');
let Model = require('../Models');
let Service = require('../Services');
const UniversalFunction = require('../Utils/UniversalFunction');

async function addUtilityCategory(utilityCategoryData, addedby, storeid, supercategoryid) {
    try {
        let query = {
            title: utilityCategoryData.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }
        let isExist = await Service.queries.findOne(Model.UtilityCategory, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE);
        }
        else {
            let dataToSave = {
                title: utilityCategoryData.title,
                storeid: storeid,
                supercategoryid: supercategoryid,
                addedby: addedby
            }
            await Service.queries.savedata(Model.UtilityCategory, dataToSave);
            return CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_ADD_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getUtilityCategory(utilitycategoryid, storeid, supercategoryid, status) {
    try {
        let utilitycategory
        if (utilitycategoryid !== null) {
            let query = {
                _id: utilitycategoryid,
                isdeleted: false,
                storeid: storeid,
                supercategoryid: supercategoryid
            }
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            utilitycategory = await Service.queries.findOne(Model.UtilityCategory, query, {}, {});
        }
        else {
            let query = {
                isdeleted: false,
                storeid: storeid,
                supercategoryid: supercategoryid
            }
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            utilitycategory = await Service.queries.find(Model.UtilityCategory, query, {}, { sort: { _id: -1 } });
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_GET_SUCCESS
        let responseData = {
            statusCode: 200,
            customMessage: successMsg.customMessage,
            data: utilitycategory
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_GET_FAILED)
    }
}

async function updateUtilityCategory(utilityCategoryData, utilitycategoryid, addedby, storeid, supercategoryid) {
    try {
        let dataToUpdate = {
            title: utilityCategoryData.title,
            storeid: storeid,
            supercategoryid: supercategoryid,
        }
        let utilitycategory = await Service.queries.findOneAndUpdate(Model.UtilityCategory, { _id: utilitycategoryid, addedby: addedby }, dataToUpdate, {});
        if (utilitycategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteUtilityCategory(utilitycategoryid, addedby) {
    try {
        let utilitycategory = await Service.queries.findOneAndUpdate(Model.UtilityCategory, { _id: utilitycategoryid, addedby: addedby }, { isdeleted: true }, {});
        if (utilitycategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function changeUtilityCategoryStatus(utilitycategoryid, addedby) {
    try {
        let utilitycategory = await Service.queries.findOne(Model.UtilityCategory, { _id: utilitycategoryid, addedby }, {}, {});
        if (utilitycategory !== null) {
            if (utilitycategory.status == 0) {
                await Service.queries.findOneAndUpdate(Model.UtilityCategory, { _id: utilitycategoryid }, { status: 1 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_CHANGE_STATUS_SUCCESS
            }
            if (utilitycategory.status == 1) {
                await Service.queries.findOneAndUpdate(Model.UtilityCategory, { _id: utilitycategoryid }, { status: 0 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_CHANGE_STATUS_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }

    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_CHANGE_STATUS_FAILED)
    }
}


async function addUtilitySubCategory(utilitySubCategoryData, addedby, storeid, supercategoryid) {
    try {
        let query = {
            title: utilitySubCategoryData.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }

        let isExist = await Service.queries.findOne(Model.UtilitySubCategory, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        else {
            let dataToSave = {
                title: utilitySubCategoryData.title,
                addedby: addedby,
                storeid: storeid,
                supercategoryid: supercategoryid,
                utilitycategoryid: utilitySubCategoryData.utilitycategoryid
            }
            await Service.queries.savedata(Model.UtilitySubCategory, dataToSave)
            return CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_ADD_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getUtilitySubCategory(utilitysubcategoryid, storeid, supercategoryid, utilitycategoryid, status) {
    try {
        let utilitysubcategory
        if (utilitysubcategoryid !== null) {
            let query = {
                _id: utilitysubcategoryid,
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            utilitysubcategory = await Service.queries.findOnePopulateData(Model.UtilitySubCategory, query, {}, {}, "utilitycategoryid");
        }
        if (utilitysubcategoryid == null) {
            let query = {
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            if (utilitycategoryid !== null) query.utilitycategoryid = utilitycategoryid
            utilitysubcategory = await Service.queries.populateData(Model.UtilitySubCategory, query, {}, { sort: { _id: -1 } }, "utilitycategoryid");
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.UTILITY_CATEGORY_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: utilitysubcategory
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_GET_FAILED)
    }
}

async function updateUtilitySubCategory(utilitySubCategoryData, utilitysubcategoryid, addedby, storeid, supercategoryid) {
    try {
        let dataToUpdate = {
            title: utilitySubCategoryData.title,
            addedby: addedby,
            storeid: storeid,
            supercategoryid: supercategoryid,
            utilitycategoryid: utilitySubCategoryData.utilitycategoryid
        }
        let utilitysubcategory = await Service.queries.findOneAndUpdate(Model.UtilitySubCategory, { _id: utilitysubcategoryid, addedby: addedby }, dataToUpdate, {});
        if (utilitysubcategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteUtilitySubCategory(utilitysubcategoryid, addedby) {
    try {
        let utilitysubcategory = await Service.queries.findOneAndUpdate(Model.UtilitySubCategory, { _id: utilitysubcategoryid, addedby: addedby }, { isdeleted: true }, {});
        if (utilitysubcategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_DELETE_FAILED)
    }
}

async function changeUtilitySubCategoryStatus(utilitysubcategoryid, addedby) {
    try {
        let utilitysubcategory = await Service.queries.findOne(Model.UtilitySubCategory, { _id: utilitysubcategoryid }, {}, {});
        if (utilitysubcategory !== null) {
            if (utilitysubcategory.status == 0) {
                await Service.queries.findOneAndUpdate(Model.UtilitySubCategory, { _id: utilitysubcategoryid }, { status: 1 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_CHANGE_STATUS_SUCCESS
            }
            if (utilitysubcategory.status == 1) {
                await Service.queries.findOneAndUpdate(Model.UtilitySubCategory, { _id: utilitysubcategoryid }, { status: 0 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_CHANGE_STATUS_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_SUBCATEGORY_CHANGE_STATUS_FAILED)
    }
}

async function addUtilities(utilitiesData, addedby) {
    try {
        let query = {
            title: utilitiesData.title,
            isdeleted: false,
            supercategoryid: utilitiesData.supercategoryid,
            storeid: utilitiesData.storeid
        }
        let isExist = await Service.queries.findOne(Model.Utilities, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        else {
            let dataToSave = {
                title: utilitiesData.title,
                description: utilitiesData.description,
                price: (utilitiesData.price).toFixed(2),
                ingredients: utilitiesData.ingredients,
                unit: utilitiesData.unit,
                utility_image: utilitiesData.utility_image,
                quantity: utilitiesData.quantity,
                storeid: utilitiesData.storeid,
                extra_ingredients: utilitiesData.extra_ingredients,
                supercategoryid: utilitiesData.supercategoryid,
                utilitycategoryid: utilitiesData.utilitycategoryid,
                utilitysubcategoryid: utilitiesData.utilitysubcategoryid,
                addedby: addedby
            }
            await Service.queries.savedata(Model.Utilities, dataToSave);
            return CONFIG.appConstants.STATUS_MSG.UTILITY_ADD_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getUtilities(utilityid, storeid, supercategoryid, queryData) {
    try {
        let query = {};
        let utilities
        let utilitycategoryid = queryData.utilitycategoryid || null
        let utilitysubcategoryid = queryData.utilitysubcategoryid || null
        let status = queryData.status
        if (utilityid !== null) {
            query = {
                _id: utilityid,
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (utilitycategoryid !== null) {
                query.utilitycategoryid = utilitycategoryid
            }
            if (utilitysubcategoryid !== null) {
                query.utilitysubcategoryid = utilitysubcategoryid
            }
            console.log(query);
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            utilities = await Service.queries.findOnePopulateData(Model.Utilities, query, {}, {}, ["storeid", "utilitycategoryid", "utilitysubcategoryid"]);
        }
        if (utilityid == null) {
            query = {
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (utilitycategoryid !== null) {
                query.utilitycategoryid = utilitycategoryid
            }
            if (utilitysubcategoryid !== null) {
                query.utilitysubcategoryid = utilitysubcategoryid
            }
            if (status == 0 || status == 1) query.status = status
            console.log(status);
            utilities = await Service.queries.populateData(Model.Utilities, query, {}, { sort: { _id: -1 } }, ["storeid", "utilitycategoryid", "utilitysubcategoryid"]);
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.UTILITY_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: utilities
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_GET_FAILED)
    }
}

async function updateUtilities(utilitiesData, utilityid, addedby) {
    try {
        let dataToUpdate = {
            title: utilitiesData.title,
            description: utilitiesData.description,
            price: (utilitiesData.price).toFixed(2),
            ingredients: utilitiesData.ingredients,
            unit: utilitiesData.unit,
            utility_image: utilitiesData.utility_image,
            quantity: utilitiesData.quantity,
            extra_ingredients: utilitiesData.extra_ingredients,
            storeid: utilitiesData.storeid,
            supercategoryid: utilitiesData.supercategoryid,
            utilitycategoryid: utilitiesData.utilitycategoryid,
            utilitysubcategoryid: utilitiesData.utilitysubcategoryid
        }
        let utilities = await Service.queries.findOneAndUpdate(Model.Utilities, { _id: utilityid, addedby: addedby }, dataToUpdate, {});
        console.log(utilities);
        if (utilities !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteUtilities(utilityid, addedby) {
    try {
        let utilities = await Service.queries.findOneAndUpdate(Model.Utilities, { _id: utilityid, addedby: addedby }, { isdeleted: true }, {});
        if (utilities !== null) {
            return CONFIG.appConstants.STATUS_MSG.UTILITY_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_DELELTE_FAILED)
    }
}

async function changeUtilitiesStatus(utilityid, addedby) {
    try {
        let utilities = await Service.queries.findOne(Model.Utilities, { _id: utilityid }, {}, {});
        if (utilities !== null) {
            if (utilities.status == 0) {
                await Service.queries.findOneAndUpdate(Model.Utilities, { _id: utilityid }, { status: 1 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_CHANGE_STATUS_SUCCESS
            }
            if (utilities.status == 1) {
                await Service.queries.findOneAndUpdate(Model.Utilities, { _id: utilityid }, { status: 0 }, {});
                return CONFIG.appConstants.STATUS_MSG.UTILITY_CHANGE_STATUS_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }

    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UTILITY_CHANGE_STATUS_FAILED)
    }
}
module.exports = {
    addUtilityCategory: addUtilityCategory,
    getUtilityCategory: getUtilityCategory,
    updateUtilityCategory: updateUtilityCategory,
    deleteUtilityCategory: deleteUtilityCategory,
    changeUtilityCategoryStatus: changeUtilityCategoryStatus,
    addUtilitySubCategory: addUtilitySubCategory,
    getUtilitySubCategory: getUtilitySubCategory,
    updateUtilitySubCategory: updateUtilitySubCategory,
    deleteUtilitySubCategory: deleteUtilitySubCategory,
    changeUtilitySubCategoryStatus: changeUtilitySubCategoryStatus,
    addUtilities: addUtilities,
    getUtilities: getUtilities,
    updateUtilities: updateUtilities,
    deleteUtilities: deleteUtilities,
    changeUtilitiesStatus: changeUtilitiesStatus
}