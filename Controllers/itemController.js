let UniversalFunction = require('../Utils/UniversalFunction');
let CONFIG = require('../Config');
let Service = require('../Services');
let Model = require('../Models');
let generator = require('generate-password');
const Controllers = require('.');
const { options } = require('joi');


async function addSuperCategory(superCategoryData, storeid) {
    try {
        let dataToSave = {
            supercategoryname: superCategoryData.supercategoryname,
            description: superCategoryData.description,
        }
        await Service.queries.savedata(Model.SuperCategory, dataToSave);
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_ADDED_SUCCESS
    }
    catch (err) {
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_ADDED_FAILED
    }
}

async function getSuperCategory() {
    try {
        let supercategory = await Service.queries.find(Model.SuperCategory, {}, {}, { sort: { _id: -1 } });
        let successMsg = CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: supercategory
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_GET_FAILED)
    }
}

async function updateSuperCategory(supercategoryid, superCategoryData) {
    try {
        let dataToUpdate = {
            supercategoryname: superCategoryData.supercategoryname,
            description: superCategoryData.description
        }
        let query = {
            _id: supercategoryid,
        }
        console.log(dataToUpdate, 'data to update')
        console.log(query, 'query')
        let supercategory = await Service.queries.findOneAndUpdate(Model.SuperCategory, query, dataToUpdate, {});
        if (supercategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_UPDATED_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_UPDATED_FAILED)
        }
    }
    catch (err) {
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_UPDATED_FAILED
    }
}

async function deleteSuperCategory(supercategoryid) {
    try {
        await Service.queries.findOneAndUpdate(Model.SuperCategory, { _id: supercategoryid }, { isdeleted: true }, {});
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_DELETED_SUCCESS
    }
    catch (err) {
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_DELETED_FAILED
    }
}

async function changeSuperCategoryStatus(supercategoryid) {
    try {
        let supercategory = await Service.queries.findOne(Model.SuperCategory, { _id: supercategoryid }, {}, {});
        if (supercategory.status == 0) {
            await Service.queries.findOneAndUpdate(Model.SuperCategory, { _id: supercategoryid }, { status: 1 }, {});
        }
        else if (supercategory.status == 1) {
            await Service.queries.findOneAndUpdate(Model.SuperCategory, { _id: supercategoryid }, { status: 1 }, {});
        }
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_CHANGE_STATUS_SUCCESS
    }
    catch (err) {
        return CONFIG.appConstants.STATUS_MSG.SUPERCATEGORY_CHANGE_STATUS_FAILED
    }
}
async function addCategory(categorydata, supercategoryid, userid, storeid) {
    try {
        let query = {
            title: categorydata.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }
        let isExist = await Service.queries.findOne(Model.Category, query, {}, {});
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        else {
            let dataToSave = {
                title: categorydata.title,
                description: categorydata.description,
                supercategoryid: supercategoryid,
                addedby: userid,
                storeid: storeid
            }
            await Service.queries.savedata(Model.Category, dataToSave);
            return CONFIG.appConstants.STATUS_MSG.CATEGORY_ADD_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getCategory(storeid, supercategoryid, categoryid,status) {
    try {
        let category
        if (categoryid !== null) {
            let query = {
                _id: categoryid,
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if(status == 0 || status == 1) query.status = status 
            console.log(status);
            category = await Service.queries.findOnePopulateData(Model.Category, query, {}, {}, ['storeid', 'supercategoryid']);
        }
        else {
            let query = {
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            } 
            if(status == 0 || status == 1) query.status = status 
            console.log(status);
            category = await Service.queries.populateData(Model.Category, query, {}, { sort: { _id: -1 } }, ['storeid', 'supercategoryid']);
        }
        console.log(category);
        let successMsg = CONFIG.appConstants.STATUS_MSG.CATEGORY_FETCH_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: category
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CATEGORY_FETCH_FAILED)
    }
}

async function updateCategory(categoryid, categorydata, supercategoryid, user) {
    try {
        let dataToUpdate = {
            title: categorydata.title,
            description: categorydata.description,
            supercategoryid: supercategoryid,
            storeid: categorydata.storeid
        }
        console.log(dataToUpdate, 'data to update')
        let category = await Service.queries.findOneAndUpdate(Model.Category, { _id: categoryid, addedby: user.id }, dataToUpdate, {});
        if (category !== null) {
            return CONFIG.appConstants.STATUS_MSG.CATEGORY_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteCategory(categoryid, addedby) {
    try {
        console.log(addedby)
        let category = await Service.queries.findOneAndUpdate(Model.Category, { _id: categoryid, addedby: addedby }, { isdeleted: true }, {});
        if (category !== null) {
            return CONFIG.appConstants.STATUS_MSG.CATEGORY_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {

        return Promise.reject(CONFIG.appConstants.STATUS_MSG.CATEGORY_DELETE_FAILED)
    }
}


async function changeStatus(categoryid, addedby) {
    try {
        let category = await Service.queries.findOne(Model.Category, { _id: categoryid, addedby: addedby }, {}, {});
        if (category) {
            if (category.status == 0) {
                await Service.queries.findOneAndUpdate(Model.Category, { _id: categoryid }, { status: 1 });
            }
            if (category.status == 1) {
                await Service.queries.findOneAndUpdate(Model.Category, { _id: categoryid }, { status: 0 });
            }
            return CONFIG.appConstants.STATUS_MSG.STATUS_CHANGE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.STATUS_CHANGE_FAILED)
    }
}
async function addSubcategory(categoryid, supercategoryid, subcategoryData, userid, storeid) {
    try {
        let query = {
            title: subcategoryData.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }
        let isExist = await Service.queries.findOne(Model.SubCategory, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        let dataToSave = {
            title: subcategoryData.title,
            description: subcategoryData.description,
            categoryid: categoryid,
            supercategoryid: supercategoryid,
            status: 0,
            addedby: userid,
            storeid: storeid
        }
        await Service.queries.savedata(Model.SubCategory, dataToSave);
        return CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_ADD_SUCCESS
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function updateSubcategory(supercategoryid, categoryid, subcategoryid, subcategoryData, userid) {
    try {
        let dataToUpdate = {
            title: subcategoryData.title,
            description: subcategoryData.description,
            categoryid: categoryid,
            supercategoryid: supercategoryid,
        }
        let subcategory = await Service.queries.findOneAndUpdate(Model.SubCategory, { _id: subcategoryid, addedby: userid }, dataToUpdate, {});
        if (subcategory !== null) {
            return CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteSubcategory(subcategoryid, addedby) {
    try {
        if (subcategoryid) {
            let subcategory = await Service.queries.findOneAndUpdate(Model.SubCategory, { _id: subcategoryid, addedby: addedby }, { isdeleted: true }, {});
            if (subcategory !== null) {
                return CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_DELETE_SUCCESS
            }
            else {
                return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
            }
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_DELETE_FAILED)
    }
}

async function changeSubCategoryStatus(subcategoryid, addedby) {
    try {
        let subcategory = await Service.queries.findOne(Model.SubCategory, { _id: subcategoryid, addedby: addedby }, {}, {});
        if (subcategory !== null) {
            if (subcategory.status == 0) {
                await Service.queries.findOneAndUpdate(Model.SubCategory, { _id: subcategoryid }, { status: 1 });
            }
            if (subcategory.status == 1) {
                await Service.queries.findOneAndUpdate(Model.SubCategory, { _id: subcategoryid }, { status: 0 });
            }
            return CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_CHANGE_STATUS_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_CHANGE_STATUS_FAILED)
    }
}

async function getSubcategory(query) {
    try {
        let storeid = query.storeid
        let supercategoryid = query.supercategoryid
        let subcategoryid = query.subcategoryid || null
        let categoryid = query.categoryid || null
        let status = query.status
        if (subcategoryid !== null) {
            let queryData = {
                _id: subcategoryid,
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (categoryid !== null) {
                queryData.categoryid = categoryid
            }
            if(status == 0 || status == 1) queryData.status = status 
            console.log(status);
            console.log(queryData, 'querydata');
            let subcategory = await Service.queries.findOnePopulateData(Model.SubCategory, queryData, {}, {}, "categoryid")
            let successMsg = CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_GET_SUCCESS
            let responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: subcategory
            }
            return responseData
        }
        else {
            let queryData = {
                storeid: storeid,
                supercategoryid: supercategoryid,
                isdeleted: false
            }
            if (categoryid !== null) {
                queryData.categoryid = categoryid
            }
            if(status == 0 || status == 1) queryData.status = status 
            console.log(status);
            let subcategory = await Service.queries.populateData(Model.SubCategory, queryData, {}, { sort: { _id: -1 } }, "categoryid")
            console.log(subcategory);
            let successMsg = CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_GET_SUCCESS
            let responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: subcategory
            }
            return responseData
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.SUBCATEGORY_GET_FAILED)
    }
}

async function addProduct(supercategoryid, subcategoryid, productData, userid, storeid, categoryid) {
    try {
        let query = {
            title: productData.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }
        let isExist = await Service.queries.findOne(Model.Product, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        let productcode = await generateProductCode();
        let dataToSave = {
            title: productData.title,
            description: productData.description,
            productcode: productcode,
            quantity: productData.quantity,
            price: (productData.price).toFixed(2),
            unit: productData.unit,
            ingredients: productData.ingredients,
            direction_to_use: productData.direction_to_use,
            product_image: productData.product_image,
            categoryid: categoryid,
            maximum_buyable_quantity : productData.maximum_buyable_quantity,
            subcategoryid: subcategoryid,
            supercategoryid: supercategoryid,
            isagerestricted : productData.isagerestricted,
            addedby: userid,
            storeid: storeid,
        }
        console.log(dataToSave, 'file data')
        await Service.queries.savedata(Model.Product, dataToSave);
        return CONFIG.appConstants.STATUS_MSG.PRODUCT_ADD_SUCCESS
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getProduct(storeid, supercategoryid, categoryid, subcategoryid, productid, queryData) {
    try {
        let products
        let options = {};
        let status = queryData.status
        let query = {
            storeid: storeid,
            supercategoryid: supercategoryid,
            isdeleted: false,
        }
        if (productid !== null) {
            query._id = productid
        }
        if (categoryid !== null) {
            query.categoryid = categoryid
        }
        if (subcategoryid !== null) {
            query.subcategoryid = subcategoryid
        }
        if(queryData.limit !== null) {
            options.limit = queryData.limit
        }
        if(queryData.pageindex !== null) {
            options.skip = queryData.pageindex * 10
        }
        if (queryData.filter == "high") {
            options.sort = { price: -1 }
        } else if (queryData.filter == "low") {
            options.sort = { price: 1 }
        } else {
            options.sort = { _id: -1 }
        }
        if(status == 0 || status == 1) query.status = status 
        console.log(status);
        console.log(options);
        console.log(query);
        products = await Service.queries.populateData(Model.Product, query, {}, options, ["subcategoryid", "supercategoryid", "storeid", "categoryid","reviewratings.addedby"]);
        let successMsg = CONFIG.appConstants.STATUS_MSG.PRODUCT_GET_SUCCESS
        let responseData = {
        statusCode: successMsg.statusCode,
        customMessage: successMsg.customMessage,
        data: products,
    }
    return responseData
}
    catch (err) {
    console.log(err);
    return Promise.reject(CONFIG.appConstants.STATUS_MSG.PRODUCT_GET_FAILED)
}
}

async function updateProduct(subcategoryid, supercategoryid, productid, productData, userid, storeid, categoryid) {
    try {

        let dataToUpdate = {
            title: productData.title,
            description: productData.description,
            quantity: productData.quantity,
            price: (productData.price).toFixed(2),
            unit: productData.unit,
            ingredients: productData.ingredients,
            direction_to_use: productData.direction_to_use,
            categoryid: categoryid,
            product_image: productData.product_image,
            subcategoryid: subcategoryid,
            supercategoryid: supercategoryid,
            maximum_buyable_quantity : productData.maximum_buyable_quantity,
            isagerestricted : productData.isagerestricted,
            addedby: userid,
            storeid: storeid
        }
        let product = await Service.queries.findOneAndUpdate(Model.Product, { _id: productid, addedby: userid }, dataToUpdate, {});
        if (product !== null) {
            return CONFIG.appConstants.STATUS_MSG.PRODUCT_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}
async function deleteProduct(productid, addedby) {
    try {
        let product = await Service.queries.findOneAndUpdate(Model.Product, { _id: productid, addedby: addedby }, { isdeleted: true });
        if (product !== null) {
            return CONFIG.appConstants.STATUS_MSG.PRODUCT_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW);
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.PRODUCT_DELETE_FAILED)
    }
}


async function generateProductCode() {
    let options = {
        sort: { _id: -1 },
        limit: 1
    }
    let products = await Service.queries.findOne(Model.Product, {}, {}, options);
    if (!products) {
        let productcode = generator.generate({
            length: 5,
            numbers: false,
            lowercase : false,
            uppercase : true
        });
        productcode = "SKU-" + productcode + "1"
        return productcode
    }
    else {
        let value = products.productcode.match(/\d/g);
        value = value.join("");
        value = Number(value) + 1
        //    console.log("number value:-"+value);
        let digit = value.toString().length;
        let remainedchar = 6 - Number(digit)
        //   console.log("remainedchar:-"+remainedchar)
        let productcode = generator.generate({
            length: remainedchar,
            numbers: false,
            lowercase : false,
            uppercase : true
        });
        productcode = "SKU-" + productcode + value
        // console.log(productcode);
        return productcode
    }
}

async function checkEmail(email) {
    let checkemail = await Service.queries.findOne(Model.Store, { email: email }, {}, {});
    return checkemail
}


async function addStore(storeData, addedby) {
    try {
        let store = await Service.queries.findOne(Model.Store, { addedby: addedby, isdeleted: false }, {}, {});
        if (store !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.STORE_ALREADY_EXIST)
        }
        else {
            let dataToSave = {
                name: storeData.name,
                email: storeData.email,
                phone: storeData.phone,
                address: storeData.address,
                address2: storeData.address2,
                state: storeData.state,
                city: storeData.city,
                postal_code: storeData.postal_code,
                country: storeData.country,
                addedby: addedby,
                supercategoryid: storeData.supercategoryid
            }
            let store = await Service.queries.savedata(Model.Store, dataToSave);
            console.log(store);
            await Service.queries.findOneAndUpdate(Model.User,{ _id : addedby },{ storeid : store._id },{});
            let successMsg = CONFIG.appConstants.STATUS_MSG.STORE_ADD_SUCCESS
            let responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: store
            }
            return responseData
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function getStore(storeid) {
    try {
        let store = {};
        let successMsg = CONFIG.appConstants.STATUS_MSG.STORE_GET_SUCCESS
        let responseData = {};
        if (storeid !== null) {
            let query = {
                _id: storeid,
                isdeleted: false,
                status : 0
            }
            let collectionOptions = {
                path: 'supercategoryid',
                select: 'supercategoryname'
            }
            store = await Service.queries.findOnePopulateData(Model.Store, query, {}, { sort: { _id: -1 } }, collectionOptions)
        }
        else {
            let query = {
                path: 'supercategoryid',
                select: 'supercategoryname'
            }
            store = await Service.queries.populateData(Model.Store, { isdeleted: false, status : 0 }, {}, { sort: { _id: -1 } }, query)
        }
        responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: store
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.STORE_GET_FAILED)
    }
}

async function getStoreForUser(storeid) {
    try {
        let store = {};
        let successMsg = CONFIG.appConstants.STATUS_MSG.STORE_GET_SUCCESS
        if (storeid !== null) {
            let query = {
                _id: storeid,
                isdeleted: false
            }
            let collectionOptions = {
                path: 'supercategoryid',
                select: 'supercategoryname'
            }
            store = await Service.queries.findOnePopulateData(Model.Store, query, {}, { sort: { _id: -1 } }, collectionOptions)
            let orders = await Service.queries.find(Model.Order,{storeid : storeid, $or : [ { order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED } ,{ order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED } ] },{},{});
            console.log(orders);
            let totalsell = 0;
            orders.map((order)=> { totalsell = totalsell + order.discounted_total });
            console.log(totalsell);
            let responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: store,
                totalsell : totalsell
            }
            return responseData
        }
        else {
            let query = {
                path: 'supercategoryid',
                select: 'supercategoryname'
            }
            store = await Service.queries.populateData(Model.Store, { isdeleted: false }, {}, { sort: { _id: -1 } }, query)
            let responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: store
            }
            return responseData
        }
        return responseData
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function updateStore(storeid, storeData, addedby) {
    try {
        let dataToUpdate = {
            name: storeData.name,
            email: storeData.email,
            phone: storeData.phone,
            address: storeData.address,
            address2: storeData.address2,
            state: storeData.state,
            city: storeData.city,
            postal_code: storeData.postal_code,
            country: storeData.country,
            supercategoryid: storeData.supercategoryid
        }
        let store = await Service.queries.findOneAndUpdate(Model.Store, { _id: storeid, addedby: addedby }, dataToUpdate, {});
        if (store !== null) {
            return CONFIG.appConstants.STATUS_MSG.STORE_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteStore(storeid, addedby) {
    try {
        let store = await Service.queries.findOneAndUpdate(Model.Store, { _id: storeid, addedby: addedby }, { isdeleted: true }, {})
        if (store !== null) {
            return CONFIG.appConstants.STATUS_MSG.STORE_DELETE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.STORE_DELETE_FAILED)
    }
}

async function changeStoreStatus(storeid, addedby) {
    try {
        let store = await Service.queries.findOne(Model.Store, { _id: storeid, addedby: addedby }, {}, {});
        console.log(store);
        if (store !== null) {
            if (store.status == 0) {
                await Service.queries.findOneAndUpdate(Model.Store, { _id: storeid }, { status: 1 }, {})
                return CONFIG.appConstants.STATUS_MSG.STORE_CHANGE_STATUS_SUCCESS
            }
            if (store.status == 1) {
                await Service.queries.findOneAndUpdate(Model.Store, { _id: storeid }, { status: 0 }, {})
                return CONFIG.appConstants.STATUS_MSG.STORE_CHANGE_STATUS_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }

    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.STORE_CHANGE_STATUS_FAILED)
    }
}

async function changeProductStatus(productid, addedby) {
    try {
        let product = await Service.queries.findOne(Model.Product, { _id: productid, addedby: addedby }, {});
        if (product !== null) {
            if (product.status == 0) {
                await Service.queries.findOneAndUpdate(Model.Product, { _id: productid }, { status: 1 }, {});
                return CONFIG.appConstants.STATUS_MSG.PRODUCT_CHANGE_STATUS_SUCCESS
            }
            if (product.status == 1) {
                await Service.queries.findOneAndUpdate(Model.Product, { _id: productid }, { status: 0 }, {});
                return CONFIG.appConstants.STATUS_MSG.PRODUCT_CHANGE_STATUS_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.PRODUCT_CHANGE_STATUS_FAILED)
    }
}

// async function generateDiscountCode(discount_code)
// {
//     let options = {
//         sort : { _id : -1 },
//         limit : 1
//     }
//     let discount = await Service.queries.findOne(Model.Discount,{},{},options);
//     if(!discount)
//     {
//         discount_code = discount_code + "1"
//         return discount_code
//     }
//     else
//     {
//             let value = discount.discount_code.match(/\d/g);
//             value = value.join("");
//             value = Number(value) + 1
//             discount_code = discount_code + value
//             return discount_code
//     }
// }

async function addDiscount(discountData, query, addedby, storeid, supercategoryid) {
    try {
        let productid = discountData.productid || null
        let categoryid = discountData.categoryid || null
        let subcategoryid = discountData.subcategoryid || null
        let storeid = discountData.storeid || null
        let query = {
            title: discountData.title,
            isdeleted: false,
            supercategoryid: supercategoryid,
            storeid: storeid
        }
        let isExist = await Service.queries.findOne(Model.Discount, query, {}, {});
        console.log(isExist);
        if (isExist !== null) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
        }
        else {
            console.log("product id :-" + discountData.productid);
            if (productid !== null && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
                console.log("product discount")
                await addDiscountToProduct(productid, discountData, "product")
            }
            else if (subcategoryid !== null && subcategoryid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
                await addDiscountToProduct(subcategoryid, discountData, "subcategory")
            }
            else if (categoryid !== null && categoryid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
                await addDiscountToProduct(categoryid, discountData, "category")
            }
            else if (storeid !== null && storeid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
                await addDiscountToProduct(storeid, discountData, "store")
            }
            let discount_code = "D" + new Date().getTime();
            console.log(discount_code);
            let dataToSave = {
                title: discountData.title,
                description: discountData.description,
                discount_code: discount_code,
                measure: discountData.measure,
                type: discountData.type,
                offer: discountData.offer,
                minimum_order_amount: discountData.minimum_order_amount,
                storeid: storeid,
                supercategoryid: discountData.supercategoryid,
                categoryid: categoryid,
                subcategoryid: subcategoryid,
                productid: productid,
                expiry_date: discountData.expiry_date,
                addedby: addedby
            }
            await Service.queries.savedata(Model.Discount, dataToSave);
            return CONFIG.appConstants.STATUS_MSG.DISCOUNT_ADD_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getDiscount(discountid, storeid, supercategoryid,queryData) {
    try {
        let discount
        if (discountid !== null) {
            let query = {
                isdeleted: false,
                storeid: storeid,
                _id: discountid
            }
            if(supercategoryid !==null) { query.supercategoryid = supercategoryid } 
            discount = await Service.queries.findOnePopulateData(Model.Discount, query, {}, {}, ["supercategoryid", "categoryid", "subcategoryid", "productid"]);
        }
        if (discountid == null) {
            console.log("discountid null")
            let query = {
                isdeleted: false,
                storeid: storeid
            }
            if(supercategoryid !==null) { query.supercategoryid = supercategoryid } 
            if(queryData.categoryid) query.categoryid = queryData.categoryid
            if(queryData.subcategoryid) query.subcategoryid = queryData.subcategoryid
            if(queryData.productid) query.productid = queryData.productid
            if(queryData.type) query.type = queryData.type
            discount = await Service.queries.populateData(Model.Discount, query, {}, { sort: { _id: -1 } }, ["supercategoryid", "categoryid", "subcategoryid", "productid"])
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.DISCOUNT_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: discount
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DISCOUNT_GET_FAILED)
    }
}

async function updateDiscount(discountData, query, addedby, storeid, supercategoryid) {
    try {
        let productid = discountData.productid || null
        let categoryid = discountData.categoryid || null
        let subcategoryid = discountData.subcategoryid || null
        let storeid = discountData.storeid || null
        if (productid !== null && productid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            await addDiscountToProduct(productid, discountData, "product")
        }
        else if (subcategoryid !== null && subcategoryid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            await addDiscountToProduct(subcategoryid, discountData, "subcategory")
        }
        else if (categoryid !== null && categoryid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            await addDiscountToProduct(categoryid, discountData, "category")
        }
        else if (storeid !== null && storeid !== '' && discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            await addDiscountToProduct(storeid, discountData, "store")
        }
        let dataToUpdate = {
            title: discountData.title,
            description: discountData.description,
            measure: discountData.measure,
            type: discountData.type,
            offer: discountData.offer,
            minimum_order_amount: discountData.minimum_order_amount,
            storeid: discountData.storeid,
            supercategoryid: discountData.supercategoryid,
            categoryid: discountData.categoryid,
            subcategoryid: discountData.subcategoryid,
            productid: discountData.productid,
            expiry_date: discountData.expiry_date,
        }
        let discount = await Service.queries.findOneAndUpdate(Model.Discount, { _id: query.discountid, addedby: addedby }, dataToUpdate, {});
        console.log(discount);
        if (discount !== null) {
            return CONFIG.appConstants.STATUS_MSG.DISCOUNT_UPDATE_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DUPLICATE)
    }
}

async function deleteDiscount(discountid, addedby) {
    try {
        let discount = await Service.queries.findOne(Model.Discount, { _id: discountid, addedby: addedby }, {}, {});
        let date = new Date();
        if (discount !== null) {
            if (discount.productid !== null) {
                await Service.queries.findOneAndUpdate(Model.Product, { _id: discount.productid }, { expiry_date: date }, {});
            }
            else if (discount.subcategoryid !== null) {
                await Service.queries.update(Model.Product, { subcategoryid: discount.subcategoryid }, { expiry_date: date }, {});
            }
            else if (discount.categoryid !== null) {
                await Service.queries.update(Model.Product, { categoryid: discount.categoryid }, { expiry_date: date }, {});
            }
            else if (discount.storeid !== null) {
                await Service.queries.update(Model.Product, { categoryid: discount.categoryid }, { expiry_date: date }, {});
            }
            discount = await Service.queries.findOneAndUpdate(Model.Discount, { _id: discountid, addedby: addedby }, { isdeleted: true }, {});
            if (discount !== null) {
                return CONFIG.appConstants.STATUS_MSG.DISCOUNT_DELETE_SUCCESS
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DISCOUNT_DELETE_FAILED)
    }
}

async function changeDiscountStatus(discountid, addedby) {
    try {
        let discounts = await Service.queries.findOne(Model.Discount, { _id: discountid, addedby: addedby }, {}, {});
        if (discounts !== null) {
            if (discounts.status == 0) {
                await Service.queries.findOneAndUpdate(Model.Discount, { _id: discountid }, { status: 1 }, {})
            }
            if (discounts.status == 1) {
                await Service.queries.findOneAndUpdate(Model.Discount, { _id: discountid }, { status: 0 }, {})
            }
            return CONFIG.appConstants.STATUS_MSG.DISCOUNT_CHANGE_STATUS_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.DISCOUNT_CHANGE_STATUS_FAILED)
    }
}

async function addDiscountToProduct(id, discountData, type) {
    console.log(discountData);
    if (type == "product") {
        if (discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            console.log("on product")
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER) {
                let product = await Service.queries.findOne(Model.Product, { _id: id }, {}, {});
                console.log("product :-" + product);
                let discount = {
                    discount_price: (product.price - discountData.offer).toFixed(2),
                    expiry_date: discountData.expiry_date
                }
                await Service.queries.findOneAndUpdate(Model.Product, { _id: id }, discount, {});
            }
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE) {
                let product = await Service.queries.findOne(Model.Product, { _id: id }, {}, {});
                console.log("percentage:-" + product)
                let discount = {
                    discount_price: (product.price - (product.price * discountData.offer) / 100).toFixed(2),
                    expiry_date: discountData.expiry_date
                }
                await Service.queries.findOneAndUpdate(Model.Product, { _id: id }, discount, {});
            }
        }
    }
    else if (type == "subcategory") {
        if (discountData.type == CONFIG.appConstants.SERVICE.OFFER_TYPE.ONPRODUCT) {
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER) {
                let products = await Service.queries.find(Model.Product, { subcategoryid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (products[i].price - discountData.offer).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE) {
                let products = await Service.queries.find(Model.Product, { subcategoryid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (productS[i].price - (products[i].price * discountData.offer) / 100).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
        }
    }
    else if (type == "category") {
        {
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER) {
                let products = await Service.queries.find(Model.Product, { categoryid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (products[i].price - discountData.offer).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE) {
                let products = await Service.queries.find(Model.Product, { categoryid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (products[i].price - (products[i].price * discountData.offer) / 100).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
        }
    }
    else if (type == "store") {
        {
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.DOLLER) {
                let products = await Service.queries.find(Model.Product, { storeid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (products[i].price - discountData.offer).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
            if (discountData.measure == CONFIG.appConstants.SERVICE.MEASURE_TYPE.PERCENTAGE) {
                let products = await Service.queries.find(Model.Product, { storeid: id }, {}, {});
                for (let i = 0; i < products.length; i++) {
                    let discount = {
                        discount_price: (products[i].price - (products[i].price * discountData.offer) / 100).toFixed(2),
                        expiry_date: discountData.expiry_date
                    }
                    await Service.queries.findOneAndUpdate(Model.Product, { _id: products[i]._id }, discount, {});
                }
            }
        }
    }
}

async function getCategoriesProduct(storeid,supercategoryid,categoryid,payload)
{
    try {
        let options = {};
        let query = {
            isdeleted : false,
            storeid : storeid,
            supercategoryid : supercategoryid
        }
        if(categoryid && categoryid.length > 0) {
            query.categoryid = { $in : categoryid }
        }
        if (payload.filter == "high") {
            options.sort = { price: -1 }
        } else if (payload.filter == "low") {
            options.sort = { price: 1 }
        } else {
            options.sort = { _id: -1 }
        }
        let products = await Service.queries.find(Model.Product,query,{},options);
        let successMsg = CONFIG.appConstants.STATUS_MSG.PRODUCT_GET_SUCCESS 
        let responseData = {
            statusCode : successMsg.statusCode,
            customMessage : successMsg.customMessage,
            data : products
        }
        return responseData
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function getProductReview(query) {
    try {
        let productid = query.productid || null
        let product = {};
        if(productid!==null) {
            product = await Service.queries.findOnePopulateData(Model.Product,{ _id : productid },{ reviewratings : 1,number_of_reviews : 1, averageratings : 1 },{},"reviewratings.addedby");
        } else {
            product = await Service.queries.populateData(Model.Product,{},{ reviewratings : 1,number_of_reviews : 1, averageratings : 1 },{},"reviewratings.addedby")
        }
        return product
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function customerReview(query) {
    try {
        let product = {};
        console.log(query.productId,"product id");
        if(query.productId) {
            product = await Service.queries.findOnePopulateData(Model.Product,{ _id : query.productId },{},{},"reviewratings.addedby");
            let customer_data = [];
            for(let i=0; i<product.reviewratings.length; i++) {
                let first_name = {};
                let last_name = {};
                product.reviewratings[i].addedby.map((data) => { first_name = data.first_name; last_name = data.last_name });
                customer_data.push({
                    title : product.title,
                    first_name : first_name,
                    last_name : last_name,
                    review : product.reviewratings[i].review,
                    ratings : product.reviewratings[i].ratings
                })
            }
            return customer_data;
        } else if(query.customerId) {
            product = await Service.queries.populateData(Model.Product,{},{},{},"reviewratings.addedby");
            let responseData = [];
            product.map((item) => {
                item.reviewratings.map((reviewrating)=> {
                    //console.log("review rating",reviewrating.addedby);
                    reviewrating.addedby.map((customer) => {
                        //console.log("customer",customer._id);
                        if(customer._id == query.customerId) {
                            responseData.push({
                                title : item.title,
                                first_name : customer.first_name,
                                last_name : customer.last_name,
                                review : reviewrating.review,
                                ratings : reviewrating.ratings,
                            })
                        }
                    })
                })
            })
            return responseData;
        } else {    
            product = await Service.queries.populateData(Model.Product,{},{},{},"reviewratings.addedby");
            let responseData = [];
            for(let i=0; i<product.length; i++) {
                for(let j=0; j<product[i].reviewratings.length; j++) {
                    let first_name = {};
                    let last_name = {};
                    product[i].reviewratings[j].addedby.map((data) => { first_name = data.first_name; last_name = data.last_name });
                    responseData.push({
                        title : product[i].title,
                        first_name : first_name,
                        last_name : last_name,
                        review : product[i].reviewratings[j].review,
                        ratings : product[i].reviewratings[j].ratings
                    })  
                }
            }
            return responseData;
        }
    } catch(err) {
        console.log(err);
        return Promise.reject(err);
    }
}
module.exports = {
    addSuperCategory: addSuperCategory,
    getSuperCategory: getSuperCategory,
    updateSuperCategory: updateSuperCategory,
    deleteSuperCategory: deleteSuperCategory,
    addCategory: addCategory,
    updateCategory: updateCategory,
    getCategory: getCategory,
    deleteCategory: deleteCategory,
    addSubcategory: addSubcategory,
    deleteSubcategory: deleteSubcategory,
    updateSubcategory: updateSubcategory,
    getSubcategory: getSubcategory,
    addProduct: addProduct,
    getProduct: getProduct,
    updateProduct: updateProduct,
    deleteProduct: deleteProduct,
    changeStatus: changeStatus,
    changeSubCategoryStatus: changeSubCategoryStatus,
    addStore: addStore,
    getStore: getStore,
    updateStore: updateStore,
    deleteStore: deleteStore,
    getStoreForUser : getStoreForUser,
    changeStoreStatus: changeStoreStatus,
    changeProductStatus: changeProductStatus,
    addDiscount: addDiscount,
    getDiscount: getDiscount,
    updateDiscount: updateDiscount,
    deleteDiscount: deleteDiscount,
    changeDiscountStatus: changeDiscountStatus,
    getCategoriesProduct : getCategoriesProduct,
    getProductReview : getProductReview,
    customerReview : customerReview
}