let UniversalFunction = require('../Utils/UniversalFunction');
let CONFIG = require('../Config');
let Service = require('../Services');
let Model = require('../Models');
const { callbackPromise } = require('nodemailer/lib/shared');
const Cart = require('../Models/Cart');
let moment = require('moment');

async function addToCart(addedby, cartData) {
    try {
        let cartExists = await Service.queries.findOne(Model.Cart, { addedby: addedby, storeid: cartData.storeid }, {}, {});
        if (cartExists) {
            let dataToUpdate = "";
            if (cartData.productid) {
                let cdate = new Date();
                let products = await Service.queries.findOne(Model.Product, { _id: cartData.productid }, {}, {});
                if (products.expiry_date && products.expiry_date > cdate) {
                    cartExists.item.unshift({
                        productid: cartData.productid,
                        quantity: cartData.quantity,
                        discounted_price: (products.discount_price * cartData.quantity).toFixed(2),
                        original_price: (products.price * cartData.quantity).toFixed(2)
                    })
                    cartExists.discounted_total = cartExists.discounted_total + (products.discount_price * cartData.quantity).toFixed(2),
                        cartExists.original_total = cartExists.original_total + (products.price * cartData.quantity).toFixed(2),
                        cartExists.addedby = addedby
                    cartExists.storeid = cartData.storeid
                }
                else {
                    cartExists.item.unshift({
                        productid: cartData.productid,
                        quantity: cartData.quantity,
                        discounted_price: (products.price * cartData.quantity).toFixed(2),
                        original_price: (products.price * cartData.quantity).toFixed(2)
                    })
                    cartExists.discounted_total = (cartExists.discounted_total + (products.price * cartData.quantity)).toFixed(2),
                        cartExists.original_total = (cartExists.original_total + (products.price * cartData.quantity)).toFixed(2),
                        cartExists.addedby = addedby
                    cartExists.storeid = cartData.storeid
                }
            } else if (cartData.utilityid) {
                let extra_ingredients_total = 0
                cartData.extra_ingredients.map((extra_ingredient) => {
                    extra_ingredients_total = (extra_ingredients_total + extra_ingredient.price).toFixed(2)
                });
                let utilities = await Service.queries.findOne(Model.Utilities, { _id: cartData.utilityid }, {}, {});
                cartExists.item.unshift({
                    utilityid: cartData.utilityid,
                    quantity: cartData.quantity,
                    extra_ingredients: cartData.extra_ingredients,
                    discounted_price: (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2),
                    original_price: (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                });
                cartExists.discounted_total = (parseInt(cartExists.discounted_total) + parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                cartExists.original_total = (parseInt(cartExists.original_total) + parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                cartExists.storeid = cartData.storeid
                cartExists.addedby = addedby
            }
            await cartExists.save();
            return CONFIG.appConstants.STATUS_MSG.ADD_TO_CART_SUCCESS
        }
        else {
            console.log("else condition");
            if (cartData.productid) {
                let cdate = new Date();
                let cart = new Cart();
                let products = await Service.queries.findOne(Model.Product, { _id: cartData.productid }, {}, {});
                let dataToSave = "";
                if (products.expiry_date && products.expiry_date > cdate) {
                    cart.item.unshift({
                        productid: cartData.productid,
                        quantity: cartData.quantity,
                        discounted_price: (products.discount_price * cartData.quantity).toFixed(2),
                        original_price: (products.price * cartData.quantity).toFixed(2)
                    })
                    cart.discounted_total = (products.discount_price * cartData.quantity).toFixed(2),
                        cart.original_total = (products.price * cartData.quantity).toFixed(2),
                        cart.addedby = addedby
                    cart.storeid = cartData.storeid
                    await cart.save();
                }
                else {
                    cart.item.unshift({
                        productid: cartData.productid,
                        quantity: cartData.quantity,
                        discounted_price: (products.price * cartData.quantity).toFixed(2),
                        original_price: (products.price * cartData.quantity).toFixed(2)
                    })
                    cart.discounted_total = (products.price * cartData.quantity).toFixed(2),
                        cart.original_total = (products.price * cartData.quantity).toFixed(2),
                        cart.addedby = addedby
                    cart.storeid = cartData.storeid
                    await cart.save();
                }
            } else if (cartData.utilityid) {
                let cart = new Cart();
                let utilities = await Service.queries.findOne(Model.Utilities, { _id: cartData.utilityid }, {}, {});
                let extra_ingredients_total = 0
                cartData.extra_ingredients.map((extra_ingredient) => {
                    extra_ingredients_total = (extra_ingredients_total + extra_ingredient.price).toFixed(2)
                })
                cart.item.unshift({
                    utilityid: cartData.utilityid,
                    quantity: cartData.quantity,
                    extra_ingredients: cartData.extra_ingredients,
                    discounted_price: (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2),
                    original_price: (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                })
                cart.discounted_total = (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                cart.original_total = (parseInt(extra_ingredients_total) + parseInt(utilities.price * cartData.quantity)).toFixed(2)
                cart.storeid = cartData.storeid
                cart.addedby = addedby
                await cart.save();
            }
            return CONFIG.appConstants.STATUS_MSG.ADD_TO_CART_SUCCESS
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.ADD_TO_CART_FAILED)
    }
}


async function getCart(user, storeid) {
    try {
        console.log(user)
        let cart = await Service.queries.findOnePopulateData(Model.Cart, { addedby: user, storeid: storeid }, {}, {}, ["item.productid", "item.utilityid"]);
        let successMsg = CONFIG.appConstants.STATUS_MSG.GET_CART_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: cart
        }
        return responseData
    }
    catch (err) {
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.GET_CART_FAILED)
    }
}

async function updateCart(cartid, addedby, cartData) {
    try {
        let dataToUpdate = {
            item: cartData.item,
            discounted_total: (cartData.discounted_total).toFixed(2),
            original_total: (cartData.original_total).toFixed(2),
            storeid: cartData.storeid
        }
        let cartUpdate = await Service.queries.findOneAndUpdate(Model.Cart, { _id: cartid, addedby: addedby }, dataToUpdate, {});
        if (cartUpdate !== null) {
            return CONFIG.appConstants.STATUS_MSG.UPDATE_CART_SUCCESS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.UPDATE_CART_FAILED)
    }
}

async function discountCodeExists(discount_code) {
    let findDiscountCode = await Service.queries.findOne(Model.Discount, { discount_code: discount_code }, {}, {});
    return findDiscountCode
}

async function alreadyUsedCode(addedby, payload) {
    try {
        let discount_code = payload.discount_code
        let discountCode = await Service.queries.findOne(Model.Discount, { discount_code: discount_code, type: CONFIG.appConstants.SERVICE.OFFER_TYPE.ONORDER, storeid: payload.storeid, isdeleted: false }, {}, {});
        if (discountCode !== null) {
            let query = {
                addedby: addedby,
                discount_code: discount_code
            }
            let validCode = await Service.queries.findOne(Model.Order, query, {}, {});
            if (validCode == null) {
                let cart = await Service.queries.findOne(Model.Cart, { _id: payload.cartid }, {}, {});
                if (cart) {
                    let discount = await UniversalFunction.getOrderTotal(discount_code, cart.discounted_total);
                    let successMsg = CONFIG.appConstants.STATUS_MSG.COUPON_APPLIED_SUCCESS
                    let responseData = {
                        statusCode: successMsg.statusCode,
                        customMessage: successMsg.customMessage,
                        data: discount
                    }
                    return responseData
                }
            }
            else {
                return Promise.reject(CONFIG.appConstants.STATUS_MSG.COUPON_ALREADY_USED)
            }
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.INVALID_DISCOUNT_CODE)
        }
    }
    catch (err) {
        return Promise.reject(err);
    }
}

async function stockAvailable(cartid, addedby) {
    let cart = await Service.queries.findOne(Model.Cart, { _id: cartid }, {}, {});
    for (let i = 0; i < cart.item.length; i++) {
        let product = await Service.queries.findOne(Model.Product, { _id: cart.item[i].productid }, {}, {});
        console.log(product.quantity);
        if (cart.item[i].quantity > product.quantity) {
            console.log(cart.item[i].quantity);
            console.log(product.quantity);
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.QUANTITY_NOT_AVAILABLE)
        }
    }
    return CONFIG.appConstants.STATUS_MSG.STOCK_AVAILABLE
}

async function placeOrder(cartid, addedby, orderData) {
    try {
        let cart = await Service.queries.findOne(Model.Cart, { _id: cartid, storeid: orderData.storeid }, {}, {});
        console.log(orderData, 'asdasd')
        // get order number
        let lastOrder = await Service.queries.findOne(Model.Order, {}, {}, { sort: { _id: -1 } })
        console.log(lastOrder, "last order details");
        console.log(lastOrder.order_number, 'last order number')
        let currentOrderNumber = lastOrder
        if (!currentOrderNumber) {
            currentOrderNumber = 1
        } else {
            currentOrderNumber = lastOrder.order_number + 1
        }
        console.log(currentOrderNumber);
        let total
        let dataToSave
        let discount_code = orderData.discount_code || null
        if (discount_code !== '' && discount_code !== null) {
            total = await UniversalFunction.getOrderTotal(orderData.discount_code, cart.discounted_total)
            dataToSave = {
                addedby: addedby,
                first_name: orderData.first_name,
                last_name: orderData.last_name,
                email: orderData.email,
                phone: orderData.phone,
                address: orderData.address,
                address2: orderData.address2,
                city: orderData.city,
                state: orderData.state,
                postal_code: orderData.postal_code,
                country: orderData.country,
                item: cart.item,
                discount_code: orderData.discount_code,
                discounted_total: (total.discounted_total).toFixed(2),
                original_total: (total.original_total).toFixed(2),
                payment_mode: orderData.payment_mode,
                storeid: orderData.storeid,
                order_number: currentOrderNumber
            }
        }
        else {
            dataToSave = {
                addedby: addedby,
                first_name: orderData.first_name,
                last_name: orderData.last_name,
                email: orderData.email,
                phone: orderData.phone,
                address: orderData.address,
                address2: orderData.address2,
                city: orderData.city,
                state: orderData.state,
                postal_code: orderData.postal_code,
                country: orderData.country,
                item: cart.item,
                discounted_total: (cart.discounted_total).toFixed(2),
                original_total: (cart.discounted_total).toFixed(2),
                payment_mode: orderData.payment_mode,
                storeid: orderData.storeid,
                order_number: currentOrderNumber
            }
        }
        let order = await Service.queries.savedata(Model.Order, dataToSave);
        await updateStock(cart.item)
        await Service.queries.remove(Model.Cart, { _id: cartid, storeid: orderData.storeid });
        let successMsg = CONFIG.appConstants.STATUS_MSG.ORDER_PLACED_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: order
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.ORDER_PLACED_FAILED)
    }
}

let checkStock = async function (cartid, addedby) {
    try {
        let cart = await Service.queries.findOne(Model.Cart, { _id: cartid, addedby: addedby }, {}, {});
        let outOfStockItems = [];
        for (let i = 0; i < cart.item.length; i++) {
            if (cart.item[i].productid) {
                let product = await Service.queries.findOne(Model.Product, { _id: cart.item[i].productid }, {}, {});
                if (cart.item[i].quantity > product.quantity) {
                    let data = {
                        productid: product._id,
                        name: product.title
                    }
                    outOfStockItems.push(data)
                }
            } else if (cart.item[i].utilityid) {
                let utilities = await Service.queries.findOne(Model.Utilities, { _id: cart.item[i].utilityid }, {}, {});
                if (cart.item[i].quantity > utilities.quantity) {
                    let data = {
                        utilityid: utilities._id,
                        name: utilities.title
                    }
                    outOfStockItems.push(data)
                } else {
                    for (let j = 0; j < utilities.extra_ingredients.length; j++) {
                        for (let k = 0; k < cart.item[i].extra_ingredients.length; k++) {
                            if (cart.item[i].extra_ingredients[k].quantity > utilities.extra_ingredients[j].quantity) {
                                let data = {
                                    _id: utilities._id,
                                    name: utilities.title,
                                    extra_ingredient_id: cart.item[i].extra_ingredients[k]._id,
                                    extra_ingredient_name: utilities.extra_ingredients[j].name
                                }
                                outOfStockItems.push(data)
                            }
                        }
                    }
                }
            }
        }
        if (outOfStockItems.length > 0) {
            let msg = CONFIG.appConstants.STATUS_MSG.OUT_OF_STOCK;
            let dataToSend = {
                statusCode: msg.statusCode,
                customMessage: msg.customMessage,
                inStock: msg.inStock,
                data: outOfStockItems
            }
            return dataToSend
        }
        else {
            let msg = CONFIG.appConstants.STATUS_MSG.IN_STOCK;
            let dataToSend = {
                statusCode: msg.statusCode,
                customMessage: msg.customMessage,
                inStock: msg.inStock,
                data: outOfStockItems
            }
            return dataToSend
        }
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR)
    }
}

let updateStock = async function (item) {
    return new Promise(async (resolve, reject) => {
        for (let i = 0; i < item.length; i++) {
            if (item[i].productid) {
                let product = await Service.queries.findOneAndUpdate(Model.Product, { _id: item[i].productid }, { $inc: { quantity: -item[i].quantity } }, {});
            }
            if (item[i].utilityid) {
                let utility = await Service.queries.findOneAndUpdate(Model.Utilities, { _id: item[i].utilityid }, { $inc: { quantity: -item[i].quantity } }, {});
                utility = await Service.queries.findOne(Model.Utilities, { _id: item[i].utilityid }, {}, {});
                for (let j = 0; j < utility.extra_ingredients.length; j++) {
                    for (let k = 0; k < item[i].extra_ingredients.length; k++) {
                        if (utility.extra_ingredients[j]._id == item[i].extra_ingredients[k]._id) {
                            utility.extra_ingredients[j].quantity = utility.extra_ingredients[j].quantity - item[i].extra_ingredients[k].quantity
                            await utility.save();
                        }
                    }
                }
            }
        }
        resolve();
    });
}

async function addDeliveryCharges(addedby, payloadData) {
    try {
        let dataToSave = {
            min_amount: payloadData.min_amount,
            max_amount: payloadData.max_amount,
            deliverycharge: payloadData.deliverycharge,
            addedby: addedby
        }
        await Service.queries.savedata(Model.DeliveryCharge, dataToSave)
        return CONFIG.appConstants.STATUS_MSG.DELIVERY_CHARGE_ADDED
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function getDeliveryChargeForUser(query) {
    try {
        let deliveryCharges = await Service.queries.find(Model.DeliveryCharge, { isdeleted: false }, {}, {})
        console.log(deliveryCharges, 'delivery charges')
        let charge = 0;
        deliveryCharges.map(item => {
            if (query.amount >= item.min_amount && query.amount <= item.max_amount) {
                charge = item.deliverycharge
            }
        })
        let successMsg = CONFIG.appConstants.STATUS_MSG.DELIVERY_CHARGE_GET
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: charge
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return Promise.reject(CONFIG.appConstants.STATUS_MSG.REQUEST_ERROR);
    }
}

async function deleteDeliverCharge(deliverychargeid, addedby) {
    try {
        let deliverycharge = await Service.queries.findOneAndUpdate(Model.DeliveryCharge, { _id: deliverychargeid, addedby: addedby }, { isdeleted: true }, {});
        console.log(deliverycharge);
        if (deliverycharge !== null) {
            return CONFIG.appConstants.STATUS_MSG.DELIVERY_CHARGE_DELETE
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW);
        }
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function getDeliveryChargeForAdmin(query) {
    try {
        let deliverycharge
        if (query.deliverychargeid) {
            let qry = {
                isdeleted: false,
                _id: query.deliverychargeid
            }
            deliverycharge = await Service.queries.findOne(Model.DeliveryCharge, qry, {}, {});
        }
        else {
            let qry = {
                isdeleted: false,
            }
            deliverycharge = await Service.queries.find(Model.DeliveryCharge, qry, {}, {});
        }
        console.log(deliverycharge)
        let successMsg = CONFIG.appConstants.STATUS_MSG.DELIVERY_CHARGE_GET
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: deliverycharge
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function updateDeliveryCharge(deliverychargeid, addedby, payloadData) {
    try {
        let dataToUpdate = {
            min_amount: payloadData.min_amount,
            max_amount: payloadData.max_amount,
            deliverycharge: payloadData.deliverycharge
        }
        let deliverycharge = await Service.queries.findOneAndUpdate(Model.DeliveryCharge, { _id: deliverychargeid, addedby: addedby }, dataToUpdate, {});
        if (deliverycharge !== null) {
            return CONFIG.appConstants.STATUS_MSG.DELIVERY_CHARGE_UPDATE
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return err;
    }
}


async function myOrder(userid, storeid, type) {
    try {
        let query = {addedby: userid, storeid: storeid}
        if(type == 'OPEN'){
            query.order_status = {$in: [CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING,CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED]}
        }
        else if(type == 'CLOSE'){
            query.order_status = 'completed'
        }
        else if(type == 'CANCEL'){
            query.order_status = 'cancelled'
        }
        let orders = await Service.queries.populateData(Model.Order, query, {}, { sort : { _id : -1 } }, ['item.productid', 'item.utilityid']);
        console.log(orders.length)
        let successMsg = CONFIG.appConstants.STATUS_MSG.ORDER_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: orders
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function addReviewRatings(payloadData, addedby) {
    try {
        let dataToSave = {
            item: payloadData.item,
            storeid: payloadData.storeid,
            addedby: addedby
        }
        let reviewrating = await Service.queries.savedata(Model.ReviewRatings, dataToSave);
        await addReviewRatingsOnProduct(reviewrating, addedby)
        return CONFIG.appConstants.STATUS_MSG.REVIEWRATING_ADDED_SUCCESS
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function addReviewRatingsOnProduct(reviewrating, addedby) {
    let item = reviewrating.item
    for (let i = 0; i < item.length; i++) {
        let product = await Service.queries.findOne(Model.Product, { _id: item[i].productid })
        console.log(item[i]._id);
        if (product.averageratings == 0) {
            let averageratings = 0;
            let dataToUpdate = {
                averageratings: item[i].ratings
            }
            product = await Service.queries.findOneAndUpdate(Model.Product, { _id: item[i].productid }, dataToUpdate, {});
            let dataPush = {
                review: item[i].review,
                ratings: item[i].ratings,
                date : new Date(),
                addedby: addedby
            }
            product.reviewratings.push(dataPush);
            if (item[i].review) {
                product.number_of_reviews = product.number_of_reviews + 1
            }
            await product.save();
        }
        else {
            let oldratings = product.averageratings * product.reviewratings.length;
            let newratings = item[i].ratings
            let totalratings = product.reviewratings.length + 1
            console.log("oldratings",oldratings);
            console.log("newratings",newratings);
            console.log("totalratings",totalratings);
            let dataToUpdate = {
                averageratings: ((oldratings + newratings) / totalratings).toFixed(1)
            }
            product = await Service.queries.findOneAndUpdate(Model.Product, { _id: item[i].productid }, dataToUpdate, {});
            // product = await Service.queries.findOne(Model.Product, { _id: item[i].productid },{},{})
            let dataPush = {
                review: item[i].review,
                ratings: item[i].ratings,
                date : new Date(),
                addedby: addedby
            }
            //product.averageratings = ((product.averageratings + item[i].ratings) / (product.number_of_reviews + 1)).toFixed(1);
            product.reviewratings.push(dataPush);
            if (item[i].review !== null) {
                product.number_of_reviews = product.number_of_reviews + 1
            }
            await product.save();
        }
    }
}

async function addPayment(payloadData, addedby) {
    try {
        let dataToSave = {
            addedby: addedby,
            orderid: payloadData.orderid,
            mode: payloadData.mode,
            storeid: payloadData.storeid,
            card_no: payloadData.card_no,
            amount: payloadData.amount,
            status: payloadData.status
        }
        await Service.queries.savedata(Model.Payment, dataToSave);
        return CONFIG.appConstants.STATUS_MSG.PAYMENT_ADDED_SUCCESS
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function getPayment(query) {
    try {
        let payments = ""
        let orderid = query.orderid || null
        let storeid = query.storeid
        if (orderid !== null) {
            payments = await Service.queries.findOne(Model.Payment, { orderid: orderid, storeid: storeid }, {}, {});
        }
        else {
            payments = await Service.queries.find(Model.Payment, { storeid: storeid }, {}, { sort: { _id: -1 } });
        }
        let successMsg = CONFIG.appConstants.STATUS_MSG.PAYMENT_GET_SUCCESS
        let responseData = {
            statusCode: successMsg.statusCode,
            customMessage: successMsg.customMessage,
            data: payments
        }
        return responseData
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function getOrder(query) {
    try {
        let orderid = query.orderid || null
        let storeid = query.storeid
        let orders
        let deliverycharge
        let responseData = {};
        let successMsg = CONFIG.appConstants.STATUS_MSG.ORDER_GET_SUCCESS
        if (orderid !== null) {
            orders = await Service.queries.findOnePopulateData(Model.Order, { _id: orderid, storeid: storeid }, {}, {}, ['item.productid', 'item.utilityid']);
            let query = {
                amount: orders.discounted_total
            }
            deliverycharge = await getDeliveryChargeForUser(query);
            orders.deliverycharge = deliverycharge.data
            responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: orders,
                deliverycharge: deliverycharge.data
            }
        }
        else {
            orders = await Service.queries.populateData(Model.Order, { storeid: storeid }, {}, { sort: { _id: -1 } }, ['item.productid', 'item.utilityid']);
            let totalOrders = await Service.queries.find(Model.Order,{storeid : storeid, $or : [ { order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED } ,{ order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED } ] },{},{});
            console.log(totalOrders);
            let totalsell = 0;
            totalOrders.map((order)=> { totalsell = totalsell + order.discounted_total });
            let pendingOrders = await Service.queries.find(Model.Order,{ storeid : storeid, order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.PENDING },{},{});
            let totalPendingOrders = 0;
            pendingOrders.map((order)=> { totalPendingOrders = totalPendingOrders + order.discounted_total });
            let cancelOrders = await Service.queries.find(Model.Order,{ storeid : storeid, order_status : CONFIG.appConstants.SERVICE.ORDER_STATUS.CANCELLED },{},{});
            let totalCancelOrders = 0;
            cancelOrders.map((order)=> { totalCancelOrders = totalCancelOrders + order.discounted_total });
            let totalMonthSell = 0;
            for(let i=0; i<totalOrders.length; i++) {
                if(moment(new Date()).isSame(totalOrders[i].updatedAt,'month') && (totalOrders[i].order_status == CONFIG.appConstants.SERVICE.ORDER_STATUS.COMPLETED || totalOrders[i].order_status == CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED)) {
                    totalMonthSell = totalMonthSell + totalOrders[i].discounted_total
                }
            }
            responseData = {
                statusCode: successMsg.statusCode,
                customMessage: successMsg.customMessage,
                data: orders,
                pendingOrdersTotal : totalPendingOrders,
                cancelOrdersTotal : totalCancelOrders, 
                currentMonthSell : totalMonthSell,
                totalSell : totalsell
            }
        }
        return responseData
    } catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

async function changeOrderStatus(payloadData) {
    try {
        let order = await Service.queries.findOneAndUpdate(Model.Order, { _id: payloadData.orderid, storeid: payloadData.storeid }, { order_status: payloadData.order_status }, {});
        if (order !== null) {
            return CONFIG.appConstants.STATUS_MSG.CHANGE_ORDER_STATUS
        }
        else {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.NOT_ALLOW)
        }
    }
    catch (err) {
        console.log(err);
        return err;
    }
}

async function cancelOrder(payload) {
    try {
        let orderid = payload.orderid
        let order = await Service.queries.findOne(Model.Order, { _id: orderid }, {}, {});
        console.log(order.order_status)
        if (order.order_status == CONFIG.appConstants.SERVICE.ORDER_STATUS.DISPATCHED) {
            return Promise.reject(CONFIG.appConstants.STATUS_MSG.CANCEL_ORDER_NOT_ALLOW)
        } else {
            await Service.queries.findOneAndUpdate(Model.Order, { _id: orderid }, { order_status: CONFIG.appConstants.SERVICE.ORDER_STATUS.CANCELLED });
            return CONFIG.appConstants.STATUS_MSG.ORDER_CANCELLED
        }
    } catch (err) {
        console.log(err);
        return Promise.reject(err);
    }
}

module.exports = {
    addToCart: addToCart,
    addReviewRatings: addReviewRatings,
    addPayment: addPayment,
    getPayment: getPayment,
    updateCart: updateCart,
    getCart: getCart,
    getOrder: getOrder,
    myOrder: myOrder,
    changeOrderStatus: changeOrderStatus,
    placeOrder: placeOrder,
    checkStock: checkStock,
    alreadyUsedCode: alreadyUsedCode,
    addDeliveryCharges: addDeliveryCharges,
    getDeliveryChargeForUser: getDeliveryChargeForUser,
    deleteDeliverCharge: deleteDeliverCharge,
    updateDeliveryCharge: updateDeliveryCharge,
    stockAvailable: stockAvailable,
    getDeliveryChargeForAdmin: getDeliveryChargeForAdmin,
    cancelOrder: cancelOrder
}