const Hapi = require('hapi');
const CONFIG = require('./Config');
const Routes = require('./routes');
const connectDB = require('./Utils/bootStrap');
const plugin = require('./Plugin');
const ngrok = require('ngrok');
const init = async () =>
{
    const server = Hapi.Server({
        port : CONFIG.dbConfig.config.PORT,
        routes : { cors : true }
    });
    try
    {
        server.start(()=> {
        });
        console.log(`Server started on ${server.info.uri}`);
        // let url = await ngrok.connect({
        //     addr : 8000
        // });
        // console.log(url);
        connectDB();
        await server.register(plugin);
        server.route(Routes);
    }
    catch(err)
    {
        console.log(err);
    }
}
init();