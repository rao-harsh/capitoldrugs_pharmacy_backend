let nodemailer = require('nodemailer');
let CONFIG = require('../Config');

exports.sendEmail = function(to,subject,content,attachments){
    let transporter = nodemailer.createTransport({
        service : 'gmail',
        auth : {
            user : CONFIG.appConstants.SERVER.ADMIN_EMAIL_ID,
            pass : CONFIG.appConstants.SERVER.ADMIN_EMAIL_PASSWORD
        }
    });

    return new Promise((resolve,reject)=> {
        let mailOptions = {
            from : CONFIG.appConstants.SERVER.ADMIN_EMAIL_ID,
            to : to,
            subject : subject,
            html : content
        }
        if(attachments){
            mailOptions.attachments = attachments
        }
        transporter.sendMail(mailOptions,function(err,data){
            if(err) { 
                console.log(err);
                reject(err);
                return CONFIG.appConstants.STATUS_MSG.EMAIL_SENT_FAILED
            }else {
                resolve();
                return CONFIG.appConstants.STATUS_MSG.EMAIL_SENT_SUCCESS
            }
        });
    });
}