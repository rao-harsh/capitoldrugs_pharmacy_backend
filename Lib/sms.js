let CONFIG = require('../Config')
let twilio = require('twilio');


exports.sendSms = function(number,content) {

    let twilioClient = new twilio.Twilio(CONFIG.twilioConfig.twilioCredentials.accountSid,CONFIG.twilioConfig.twilioCredentials.authToken);
    console.log(twilioClient,number);
    twilioClient.messages
            .create({
                body: content,
                from: CONFIG.twilioConfig.twilioCredentials.from,
                to: number
            })
            .then((message) => console.log(message));
};
