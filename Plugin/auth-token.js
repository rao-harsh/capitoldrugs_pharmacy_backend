'use strict'

const jwt = require('jsonwebtoken');
const CONFIG = require('../Config');

exports.plugin = 
{
    name : 'auth-token-plugin',
    register : async (server,options) => 
    {
        server.register(require('hapi-auth-bearer-token'));
        server.auth.strategy('UserAuth', 'bearer-access-token', {
            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validate : async function(request,token,reply){
                if(!token){
                    return "No token found"
                }
                else
                {
                    try
                    {
                        const result = jwt.verify(token, CONFIG.appConstants.SERVER.JWT_SECRET_KEY);
                        request.user = result.user
                        console.log(request.user);
                        const isValid = true
                        const credentials = { userDetails : token }
                        const artifacts = { 'info' : 'success' }
                        return { isValid,credentials,artifacts }
                    }
                    catch(err){
                        return "Token is not valid"
                    }
                }
            }
        });
    }
}